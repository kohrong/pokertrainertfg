package josue.pokertrainer.model;

import java.util.ArrayList;

public class Board {
   private Card[] flop;
   private Card turn;
   private Card river;
   private Street street;

    public Board(Card[] flop, Card turn, Card river) {
        this.flop = flop;
        this.turn = turn;
        this.river = river;
        street = Street.RIVER;
    }

    public Board(Card[] flop, Card turn) {
        this.flop = flop;
        this.turn = turn;
        this.river = null;
        street = Street.TURN;
    }

    public Board(Card[] flop) {
        this.flop = flop;
        this.turn = null;
        this.river = null;
        street = Street.FLOP;
    }

    public Board() {
		this.flop = new Card[3];
		this.turn = null;
		this.river = null;
		street = Street.PREFLOP;
	}

	public Card[] getFlop() {
        return flop;
    }

    public Card getTurn() {
        return turn;
    }

    public Card getRiver() {
        return river;
    }
    
    public Street getStreet(){
    	return street;
    }

	public boolean isRiver() {
		return street == Street.RIVER;
	}

	public boolean isPreFlop() {
		return street == Street.PREFLOP;
	}
	
	public boolean isTurn(){
		return street == Street.TURN;
	}
	
	public boolean isFlop(){
		return street == Street.FLOP;
	}

	public void setFlop(Card[] cards) {
		for(int i = 0; i < 3; i++)	this.flop[i] = cards[i];
		this.street = Street.FLOP;
	}

	private void setTurn(Card turn) {
		this.turn = turn;
		this.street = Street.TURN;
	}

    private void setRiver(Card river){
		this.river = river;
		this.street = Street.RIVER;
	}

	public int size() {
		if(isPreFlop())	return 0;
		if(isFlop())	return 3;
		if(isTurn())	return 4;
		return 5;
	}

	public Card get(int i) {
		if(i < 3)	return getFlop()[i];
		if(i == 3)	return getTurn();
		return getRiver();
	}

    public void setCard(Card card) {
        if (street == Street.FLOP)  setTurn(card);
        else if(street == Street.TURN)  setRiver(card);
    }

    public ArrayList<Card> getCards() {
        ArrayList<Card> cardList = new ArrayList<>();
        for (int i = 0 ; i < this.size(); i++){
            cardList.add(this.get(i));
        }
        return cardList;
    }

    public void removeLastStreet() {
        if(street == Street.FLOP){
            for (int i = 0; i < flop.length; i++)   flop[i] = null;
            this.street = Street.PREFLOP;
        }
        else if (street == Street.TURN){
            turn = null;
            this.street = Street.FLOP;
        }
        else if (street == Street.RIVER){
            river = null;
            this.street = Street.TURN;
        }

    }
}
