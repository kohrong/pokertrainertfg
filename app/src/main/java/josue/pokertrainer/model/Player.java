package josue.pokertrainer.model;

public class Player {
	private String name;
	private TexasHand holeCards;
	private PokerHand pokerHand;

	public Player(String name, TexasHand holeCards) {
		super();
		this.name = name;
		this.holeCards = holeCards;
	}

	public String getName() {
		return this.name;
	}

	public PokerHand getPokerHand() {
		return pokerHand;
	}

	public void setPokerHand(PokerHand pokerHand) {
		this.pokerHand = pokerHand;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TexasHand getTexasHand() {
		return holeCards;
	}

	public void setTexasHand(TexasHand texasHand) {
		this.holeCards = texasHand;
	}
	
}
