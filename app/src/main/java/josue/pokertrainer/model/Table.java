package josue.pokertrainer.model;

public class Table {
    private Board board;
    private Deck deck;

    public Table(Board board, Deck deck) {
        this.board = board;
        this.deck = deck;
    }
	
	public Board getBoard() {
		return board;
	}
	
	public void setBoard(Board board) {
		this.board = board;
	}
	
	public Deck getDeck() {
		return deck;
	}
	
	public void setDeck(Deck deck) {
		this.deck = deck;
	}
}
