package josue.pokertrainer.model;

/**
 * Created by Josue on 15/09/14.
 */
public class Scene {
    private Player player;
    private Table table;

    public Scene(Player player, Table table) {
        this.player = player;
        this.table = table;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }
}
