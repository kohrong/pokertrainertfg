package josue.pokertrainer.model.indicators;

public class Indicator {
    protected String id;
    protected String indicatorName;

    public String getIndicator(){
        return id;
    }
    public void setIndicator(String id){
        this.id = id;
    }
    public String getIndicatorName(){return indicatorName;}
}
