package josue.pokertrainer.model.indicators;

/**
 * Created by Josue on 15/09/14.
 */
public class HandCategoryIndicator extends Indicator {
    public HandCategoryIndicator(String id) {
        super.id = id;
        super.indicatorName = "HandCategory";
    }
}
