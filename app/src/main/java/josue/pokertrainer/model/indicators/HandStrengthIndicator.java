package josue.pokertrainer.model.indicators;

/**
 * Created by Josue on 15/09/14.
 */
public class HandStrengthIndicator extends Indicator{
    public HandStrengthIndicator(String id) {
        super.id = id;
        super.indicatorName = "HandStrength";
    }
}
