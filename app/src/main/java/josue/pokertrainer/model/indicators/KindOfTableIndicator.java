package josue.pokertrainer.model.indicators;

/**
 * Created by Josue on 15/09/14.
 */
public class KindOfTableIndicator extends Indicator {
    public KindOfTableIndicator(String id) {
        super.id = id;
        super.indicatorName = "KindOfTable";
    }
}
