package josue.pokertrainer.model.indicators;

/**
 * Created by Josue on 15/09/14.
 */
public class PokerHandIndicator extends Indicator{
    public PokerHandIndicator(String id) {
        super.id = id;
        super.indicatorName = "PokerHand";
    }
}
