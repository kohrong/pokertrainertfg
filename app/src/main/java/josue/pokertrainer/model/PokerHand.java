package josue.pokertrainer.model;

import josue.pokertrainer.checkers.handCategoryChecker.HandCategory;
import josue.pokertrainer.checkers.handCategoryChecker.HandCategoryProvider;
import josue.pokertrainer.utils.PokerHandSorter;

public final class PokerHand implements Hand{
    
    private Card[] cards = new Card[5];

    public PokerHand(Card[] cards) {
        this.cards = cards;

        PokerHandSorter pokerHandSorter = new PokerHandSorter();
        pokerHandSorter.sort(this.cards);
        this.cards = pokerHandSorter.getCards();
        checkSpecialStraight(this);
    }

    public int getSize(){
        return cards.length;
    }

    public Card getCard(int i) {
        return cards[i];
    }

    public Card[] getCards() {
        return cards;
    }

    public void setCards(Card[] cardsInOrder) {
        this.cards = cardsInOrder;
    }

    public void setCard(int index, Card card) {
        this.cards[index] = card;
    }

    private void checkSpecialStraight(PokerHand pokerHand) {
        if(new HandCategoryProvider().getHandCategory(pokerHand) == HandCategory.STRAIGHT ||
                new HandCategoryProvider().getHandCategory(pokerHand) == HandCategory.STRAIGHT_FLUSH){
            if(pokerHand.getCard(0).isAce() && pokerHand.getCard(1).getNumber() == 5){
                Card card = pokerHand.getCard(0);
                for (int i = 0; i < pokerHand.getSize()-1; i++) {
                    pokerHand.setCard(i, pokerHand.getCard(i+1));
                }
                pokerHand.setCard(4, card);
            }
        }
    }
}
