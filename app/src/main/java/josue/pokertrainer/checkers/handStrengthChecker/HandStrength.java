package josue.pokertrainer.checkers.handStrengthChecker;

import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.TexasHand;

/**
 * Created by Josue on 20/08/14.
 */
public class HandStrength {

    public enum Strength { nothing, weak, medium, strong, veryStrong }

    public Strength getStrength(TexasHand texasHand, Board board) throws Card.Exception {
        return Strength.values()[EducaPokerHandStrengthChecker.getStrength(board, texasHand)];
    }

    public String toString(Strength strength){
        switch (strength){
            case nothing:
                return "nothing";
            case weak:
                return "weak";
            case medium:
                return "medium";
            case strong:
                return "strong";
            case veryStrong:
                return "veryStrong";
        }
        return null;
    }
}
