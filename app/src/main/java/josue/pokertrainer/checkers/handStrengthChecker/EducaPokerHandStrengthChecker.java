package josue.pokertrainer.checkers.handStrengthChecker;

import josue.pokertrainer.checkers.subHandCategoryChecker.SubHandCategoryProvider;
import josue.pokertrainer.checkers.subKindOfTableChecker.SubKindOfTableProvider;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.TexasHand;

public class EducaPokerHandStrengthChecker {

    private static int [][] strength = new int [][] {
        { 4, 4, 3, 2, 2, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 4, 4, 4, 3, 2, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 4, 4, 3, 3, 3, 3, 3, 2, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 4, 4, 4, 4, 4, 4, 4, 3, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 4, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 0},
        { 4, 4, 3, 3, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2, 2, 1, 1, 1, 0},
        { 4, 4, 4, 4, 4, 4, 3, 3, 3, 3, 3, 3, 2, 2, 2, 1, 1, 1, 0},
        { 4, 4, 4, 4, 4, 4, 4, 4, 4, 3, 3, 3, 2, 2, 2, 1, 1, 1, 0},
        { 4, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 0},
        { 4, 4, 3, 3, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2, 2, 2, 1, 1, 0},
        { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 3, 3, 3, 2, 2, 1, 1, 0}
    };

    public static int getStrength(Board board, TexasHand texasHand) throws Card.Exception {
        int table = getKindOfTable(board);
        int hand = getSubHandCategory(texasHand, board);

        return strength[table][hand];
    }

    private static int getKindOfTable(Board board) {
        return new SubKindOfTableProvider().getSubKindOfTableRank(board);
    }

    private static int getSubHandCategory(TexasHand texasHand, Board board) throws Card.Exception {
        return new SubHandCategoryProvider().getSubHandCategoryRank(texasHand, board);
    }


}
