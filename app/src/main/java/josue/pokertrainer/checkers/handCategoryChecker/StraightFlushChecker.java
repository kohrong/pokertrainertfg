package josue.pokertrainer.checkers.handCategoryChecker;

import josue.pokertrainer.model.PokerHand;

public class StraightFlushChecker implements HandCategoryChecker {

    @Override
    public boolean check(PokerHand pokerHand) {
        StraightChecker straightChecker = new StraightChecker();
        FlushChecker flushChecker = new FlushChecker();
        return (straightChecker.check(pokerHand) && flushChecker.check(pokerHand));
    }

    @Override
    public HandCategory getHandCategory() {
        return HandCategory.STRAIGHT_FLUSH;
    }

}
