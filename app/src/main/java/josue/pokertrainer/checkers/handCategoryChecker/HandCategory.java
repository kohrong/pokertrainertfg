package josue.pokertrainer.checkers.handCategoryChecker;

/**
 * Created by Josue on 11/09/14.
 */
public enum HandCategory {
    STRAIGHT_FLUSH("straightFlush"),
    FOUR_OF_A_KIND("fourOfAKind"),
    FULL_HOUSE("fullHouse"),
    FLUSH("flush"),
    STRAIGHT("straight"),
    THREE_OF_A_KIND("threeOfAKind"),
    TWO_PAIR("twoPair"),
    ONE_PAIR("onePair"),
    HIGH_CARD("highCard");

    private String categoryName;

    HandCategory(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryName() {
        return categoryName;
    }
}
