package josue.pokertrainer.checkers.handCategoryChecker;


import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.PokerHand;

public class StraightChecker implements HandCategoryChecker {

    @Override
    public boolean check(PokerHand pokerHand) {
        return isConnected(pokerHand);
    }

    @Override
    public HandCategory getHandCategory() {
        return HandCategory.STRAIGHT;
    }

    private boolean isConnected(PokerHand pokerHand) {
        for (int i = 0; i < pokerHand.getSize()-1; i++) {
            Card card = pokerHand.getCard(i);
            for (int j = i+1; j < pokerHand.getSize(); j++) {
                if(!card.isConnected(pokerHand.getCard(j))) return false;
            }
        }
        return true;
    }
    
}
