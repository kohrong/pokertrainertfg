package josue.pokertrainer.checkers.handCategoryChecker;


import josue.pokertrainer.model.PokerHand;

public interface HandCategoryChecker {
    public boolean check(PokerHand pokerHand);
    public HandCategory getHandCategory();
}
