package josue.pokertrainer.checkers.handCategoryChecker;


import josue.pokertrainer.model.PokerHand;

public class HighCardChecker implements HandCategoryChecker {
    
    @Override
    public boolean check(PokerHand pokerHand){
        HandCategoryChecker[] handCategoryCheckers = new HandCategoryProvider().getHandCategoryCheckers();
        for(int i = 0; i < handCategoryCheckers.length - 1; i++){
            if(handCategoryCheckers[i].check(pokerHand)) return false;
        }
        return true;
    }

    @Override
    public HandCategory getHandCategory() {
        return HandCategory.HIGH_CARD;
    }
}
