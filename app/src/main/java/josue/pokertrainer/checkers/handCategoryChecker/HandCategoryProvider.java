package josue.pokertrainer.checkers.handCategoryChecker;


import josue.pokertrainer.model.PokerHand;

/**
 * Created by Josue on 10/09/14.
 */
public class HandCategoryProvider {

    private HandCategoryChecker[] handCategoryCheckers = new HandCategoryChecker[]{new StraightFlushChecker(),
    new FourOfAKindChecker(), new FullHouseChecker(), new FlushChecker(), new StraightChecker(),
    new ThreeOfAKindChecker(), new TwoPairChecker(), new OnePairChecker(), new HighCardChecker()};

    public HandCategory getHandCategory(PokerHand pokerHand){
        for (HandCategoryChecker handCategoryChecker : handCategoryCheckers){
            if(handCategoryChecker.check(pokerHand))
                return handCategoryChecker.getHandCategory();
        }
        return null;
    }

    public int getHandCategoryRank(PokerHand pokerHand){
        for (int i = 0; i < handCategoryCheckers.length; i++){
            if(handCategoryCheckers[i].check(pokerHand))
                return (handCategoryCheckers.length - i);
        }
        return -1;
    }

    public  HandCategoryChecker[] getHandCategoryCheckers(){
        return handCategoryCheckers;
    }
}
