package josue.pokertrainer.checkers.projectsChecker;

import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.TexasHand;

/**
 * Created by Josue on 08/10/14.
 */
public class NonProjectChecker implements ProjectChecker {
    @Override
    public boolean check(TexasHand texasHand, Board board) {
        return true;
    }

    @Override
    public Project getProject() {
        return Project.NON_PROJECTS;
    }
}
