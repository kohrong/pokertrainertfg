package josue.pokertrainer.checkers.projectsChecker;

import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.TexasHand;
import josue.pokertrainer.utils.FlushProject;
import josue.pokertrainer.utils.StraightProject;

/**
 * Created by Josue on 08/10/14.
 */
public class BothProjectsChecker implements ProjectChecker {
    @Override
    public boolean check(TexasHand texasHand, Board board) {
        return isFlushProject(texasHand, board) && isStraightProject(texasHand, board);
    }

    private boolean isStraightProject(TexasHand texasHand, Board board) {
        return new StraightProject(board, texasHand).isStraightProject();
    }

    private boolean isFlushProject(TexasHand texasHand, Board board) {
        return new FlushProject(board, texasHand).isFlushProject();
    }

    @Override
    public Project getProject() {
        return Project.BOTH_PROJECTS;
    }
}
