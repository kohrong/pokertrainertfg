package josue.pokertrainer.checkers.projectsChecker;

import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.TexasHand;
import josue.pokertrainer.utils.StraightProject;

/**
 * Created by Josue on 08/10/14.
 */
public class StraightProjectChecker implements ProjectChecker {
    @Override
    public boolean check(TexasHand texasHand, Board board) {
        return isStraightProject(texasHand, board);
    }

    private boolean isStraightProject(TexasHand texasHand, Board board) {
        return new StraightProject(board, texasHand).isStraightProject();
    }

    @Override
    public Project getProject() {
        return Project.STRAIGHT_PROJECT;
    }
}
