package josue.pokertrainer.checkers.projectsChecker;

/**
 * Created by Josue on 08/10/14.
 */
public enum Project {
    FLUSH_PROJECT("flushProject"),
    STRAIGHT_PROJECT("straightProject"),
    BOTH_PROJECTS("bothProjects"),
    NON_PROJECTS("nonProjects");

    private String projectName;

    Project(String projectName) { this.projectName = projectName; }

    public String getProjectName() { return projectName; }
}
