package josue.pokertrainer.checkers.projectsChecker;

import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.TexasHand;

/**
 * Created by Josue on 08/10/14.
 */
public class ProjectsProvider {

    private ProjectChecker[] projectCheckers = new ProjectChecker[]{new BothProjectsChecker(),
            new FlushProjectChecker(), new StraightProjectChecker(), new NonProjectChecker()};

    public Project getProject(TexasHand texasHand, Board board){
        for (ProjectChecker projectChecker : projectCheckers){
            if (projectChecker.check(texasHand, board))
                return projectChecker.getProject();
        }
        return null;
    }

}
