package josue.pokertrainer.checkers.projectsChecker;

import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.TexasHand;

/**
 * Created by Josue on 08/10/14.
 */
public interface ProjectChecker {
    public boolean check(TexasHand texasHand, Board board);
    public Project getProject();
}
