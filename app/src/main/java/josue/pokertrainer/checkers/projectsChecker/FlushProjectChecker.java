package josue.pokertrainer.checkers.projectsChecker;

import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.TexasHand;
import josue.pokertrainer.utils.FlushProject;

/**
 * Created by Josue on 08/10/14.
 */
public class FlushProjectChecker implements ProjectChecker {
    @Override
    public boolean check(TexasHand texasHand, Board board) {
        return isFlushProject(texasHand, board);
    }

    private boolean isFlushProject(TexasHand texasHand, Board board) {
        return new FlushProject(board, texasHand).isFlushProject();
    }

    @Override
    public Project getProject() {
        return Project.FLUSH_PROJECT;
    }
}
