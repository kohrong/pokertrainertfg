package josue.pokertrainer.checkers.subHandCategoryChecker;

import josue.pokertrainer.checkers.handCategoryChecker.HandCategory;
import josue.pokertrainer.checkers.handCategoryChecker.HandCategoryProvider;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.PokerHand;
import josue.pokertrainer.model.TexasHand;
import josue.pokertrainer.utils.PokerHandCalculator;

/**
 * Created by Josue on 26/08/14.
 */
public class FullHouseChecker implements SubHandCategoryChecker {
    @Override
    public boolean check(TexasHand texasHand, Board board) throws Card.Exception {
        PokerHand pokerHand = new PokerHandCalculator().calculate(texasHand, board);
        if(new HandCategoryProvider().getHandCategory(pokerHand) == HandCategory.FULL_HOUSE)  return true;

        return false;
    }

    @Override
    public SubHandCategory getSubHandCategory() {
        return SubHandCategory.FULL_HOUSE;
    }
}
