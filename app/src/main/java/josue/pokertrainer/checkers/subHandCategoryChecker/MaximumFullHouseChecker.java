package josue.pokertrainer.checkers.subHandCategoryChecker;


import java.util.ArrayList;

import josue.pokertrainer.cardListAttributes.Pairs;
import josue.pokertrainer.checkers.handCategoryChecker.HandCategory;
import josue.pokertrainer.checkers.handCategoryChecker.HandCategoryProvider;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.PokerHand;
import josue.pokertrainer.model.TexasHand;
import josue.pokertrainer.utils.PokerHandCalculator;

/**
 * Created by Josue on 26/08/14.
 */
public class MaximumFullHouseChecker implements SubHandCategoryChecker {
    @Override
    public boolean check(TexasHand texasHand, Board board) throws Card.Exception {
        PokerHand pokerHand = new PokerHandCalculator().calculate(texasHand, board);
        if (new HandCategoryProvider().getHandCategory(pokerHand) == HandCategory.STRAIGHT_FLUSH
                || new HandCategoryProvider().getHandCategory(pokerHand) == HandCategory.FOUR_OF_A_KIND)  return true;
        if(new HandCategoryProvider().getHandCategory(pokerHand) != HandCategory.FULL_HOUSE) return false;

        ArrayList<Card> sequence = createSequence(board);
        int maxValue = getMaxValue(board);
        if(new Pairs().check(sequence, 1)){
            return maxValue == texasHand.getCard0().getValue() && maxValue == texasHand.getCard1().getValue();
        }
        else if (new Pairs().check(sequence, 2)){
            return maxValue == texasHand.getCard0().getValue() || maxValue == texasHand.getCard1().getValue();
        }

        return false;
    }

    @Override
    public SubHandCategory getSubHandCategory() {
        return SubHandCategory.MAXIMUM_FULL_HOUSE;
    }

    private int getMaxValue(Board board) {
        int maxValue = board.get(0).getValue();
        for (int i = 1; i < board.size(); i++) {
            if(maxValue < board.get(i).getValue())
                maxValue = board.get(i).getValue();
        }

        return maxValue;
    }

    private ArrayList<Card> createSequence(Board board) {
        ArrayList<Card> sequence = new ArrayList<>();
        for (int i = 0; i < board.size(); i++){
            sequence.add(board.get(i));
        }
        return sequence;
    }
}
