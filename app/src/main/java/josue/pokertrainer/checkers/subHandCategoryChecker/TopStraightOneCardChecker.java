package josue.pokertrainer.checkers.subHandCategoryChecker;

import java.util.ArrayList;

import josue.pokertrainer.checkers.handCategoryChecker.HandCategory;
import josue.pokertrainer.checkers.handCategoryChecker.HandCategoryProvider;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.PokerHand;
import josue.pokertrainer.model.TexasHand;
import josue.pokertrainer.utils.PokerHandCalculator;
import josue.pokertrainer.utils.PokerHandComparator;

/**
 * Created by Josue on 28/08/14.
 */
public class TopStraightOneCardChecker implements SubHandCategoryChecker {
    @Override
    public boolean check(TexasHand texasHand, Board board) throws Card.Exception {
        if(board.isPreFlop() || board.isFlop())   return false;

        PokerHand pokerHand = new PokerHandCalculator().calculate(texasHand, board);
        if(new HandCategoryProvider().getHandCategory(pokerHand) != HandCategory.STRAIGHT)    return false;

        Card[] hand = getTopStraight(board);

        if(!isInHand(texasHand.getCard0(), hand) && !isInHand(texasHand.getCard1(), hand))
            return false;

        return new PokerHandComparator().compareTo(pokerHand, new PokerHand(hand)) == 0;
    }

    @Override
    public SubHandCategory getSubHandCategory() {
        return SubHandCategory.TOP_STRAIGHT_ONE_CARD;
    }

    private boolean isInHand(Card card1, Card[] hand) {
        for (Card card : hand){
            if (card1.getValue() == card.getValue())    return true;
        }
        return false;
    }

    private Card[] getTopStraight(Board board) throws Card.Exception {
        int maxValueConnected = getMaxValueConnected(board);
        Card[] cards = cardsAroundMaxValue(maxValueConnected, board);
        return fillStraight(cards);
    }

    private Card[] fillStraight(Card[] cards) throws Card.Exception {
        Card[] handCards = new Card[5];
        int lowCardValue;
        if(cards[0].isAce() && cards[1].getValue() < 6) {
            lowCardValue = 1;
        }
        else{
            lowCardValue = cards[3].getValue();
        }
        for (int i = 0; i < handCards.length; i++) {
            handCards[i] = new Card(lowCardValue, CardSuit.SUITS[i%4]);
            lowCardValue++;
        }
        return handCards;
    }

    private Card[] cardsAroundMaxValue(int maxValueConnected, Board board) {
        Card[] cards = new Card[4];
        int index = 0;
        int value = maxValueConnected;
        while(index < 4){
            if (isValue(value, board)) {
                cards[index] = getValue(value, board);
                index++;
            }
            value--;
        }
        return cards;
    }

    private Card getValue(int value, Board board){
        for (int i = 0; i < board.size(); i++) {
            if(value == board.get(i).getValue())    return board.get(i);
        }
        return null;
    }

    private boolean isValue(int value, Board board) {
        for (int i = 0; i < board.size(); i++) {
            if(value == board.get(i).getValue())    return true;
        }
        return false;
    }

    private int getMaxValueConnected(Board board) throws Card.Exception {
        int value = 0;
        ArrayList<Integer> discardedValues = new ArrayList<>();
        while(value == 0){
            value = getMaxValue(board, discardedValues);
            if(!isConnectedWithThreeCards(value, board)){
                discardedValues.add(value);
                value = 0;
            }
        }
        return value;
    }

    private boolean isConnectedWithThreeCards(int value, Board board) throws Card.Exception {
        int connections = 0;
        for (int i = 0; i < board.size(); i++) {
            if(connections == 3)    return true;
            if(board.get(i).isConnected(new Card(value, CardSuit.CLUB))){
                connections++;
            }
        }
        return connections == 3;
    }

    private int getMaxValue(Board board, ArrayList<Integer> discardedValues) {
        int maxValue = 0;
        for (int i = 0; i < board.size(); i++) {
            if(!isInDiscardedValues(discardedValues, board.get(i).getValue())){
                if(board.get(i).getValue() > maxValue)
                    maxValue = board.get(i).getValue();
            }
        }
        return maxValue;
    }

    private boolean isInDiscardedValues(ArrayList<Integer> discardedValues, int value) {
        for (Integer discards : discardedValues){
            if(discards == value)   return true;
        }
        return false;
    }
}
