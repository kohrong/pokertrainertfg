package josue.pokertrainer.checkers.subHandCategoryChecker;

import josue.pokertrainer.checkers.handCategoryChecker.HandCategory;
import josue.pokertrainer.checkers.handCategoryChecker.HandCategoryProvider;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.PokerHand;
import josue.pokertrainer.model.TexasHand;
import josue.pokertrainer.utils.PokerHandCalculator;

/**
 * Created by Josue on 25/08/14.
 */
public class BestTwoPairChecker implements SubHandCategoryChecker {
    @Override
    public boolean check(TexasHand texasHand, Board board) {
        PokerHand pokerHand = new PokerHandCalculator().calculate(texasHand, board);
        if(new HandCategoryProvider().getHandCategory(pokerHand) != HandCategory.TWO_PAIR)    return false;

        int maxValue = getMaxValue(board);
        int secondValue = getSecondValue(board, maxValue);

        return isValuePaired(texasHand, board, maxValue) && isValuePaired(texasHand, board, secondValue);

    }

    @Override
    public SubHandCategory getSubHandCategory() {
        return SubHandCategory.BEST_TWO_PAIR;
    }

    private int getSecondValue(Board board, int maxValue) {
        int secondValue = 0;
        for (int i = 0; i < board.size(); i++) {
            if(secondValue < board.get(i).getValue() && board.get(i).getValue() != maxValue)
                secondValue = board.get(i).getValue();
        }

        return secondValue;
    }

    private int getMaxValue(Board board) {
        int maxValue = board.get(0).getValue();
        for (int i = 1; i < board.size(); i++) {
            if(maxValue < board.get(i).getValue())
                maxValue = board.get(i).getValue();
        }

        return maxValue;
    }

    private boolean isValuePaired(TexasHand texasHand, Board board, int value) {
        int occurrences = 0;

        if(texasHand.getCard0().getValue() == value) occurrences++;
        if(texasHand.getCard1().getValue() == value) occurrences++;
        for (int i = 0; i < board.size(); i++) {
            if(board.get(i).getValue() == value) occurrences++;
        }

        return occurrences == 2;
    }
}
