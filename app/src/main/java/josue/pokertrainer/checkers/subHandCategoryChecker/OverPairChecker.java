package josue.pokertrainer.checkers.subHandCategoryChecker;

import josue.pokertrainer.checkers.handCategoryChecker.HandCategory;
import josue.pokertrainer.checkers.handCategoryChecker.HandCategoryProvider;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.PokerHand;
import josue.pokertrainer.model.TexasHand;
import josue.pokertrainer.utils.PokerHandCalculator;

/**
 * Created by Josue on 22/08/14.
 */
public class OverPairChecker implements SubHandCategoryChecker {

    @Override
    public boolean check(TexasHand texasHand, Board board) {
        PokerHand pokerHand = new PokerHandCalculator().calculate(texasHand, board);
        if(new HandCategoryProvider().getHandCategory(pokerHand)!= HandCategory.ONE_PAIR)
            return false;
        if(!texasHand.isPocketPair())   return false;
        return isOverPair(texasHand, board);
    }

    @Override
    public SubHandCategory getSubHandCategory() {
        return SubHandCategory.OVERPAIR;
    }

    private boolean isOverPair(TexasHand texasHand, Board board) {
        for (int i = 0; i < board.size(); i++) {
            if(board.get(i).getValue() > texasHand.getCard0().getValue())   return false;
        }
        return true;
    }
}
