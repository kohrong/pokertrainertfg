package josue.pokertrainer.checkers.subHandCategoryChecker;

import josue.pokertrainer.checkers.handCategoryChecker.HandCategory;
import josue.pokertrainer.checkers.handCategoryChecker.HandCategoryProvider;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.PokerHand;
import josue.pokertrainer.model.TexasHand;
import josue.pokertrainer.utils.PokerHandCalculator;

/**
 * Created by Josue on 25/08/14.
 */
public class MaxPairSecondKickerChecker implements SubHandCategoryChecker {

    @Override
    public boolean check(TexasHand texasHand, Board board) {
        PokerHand pokerHand = new PokerHandCalculator().calculate(texasHand, board);
        if(new HandCategoryProvider().getHandCategory(pokerHand) != HandCategory.ONE_PAIR)   return false;

        int maxBoardValue = getMaxValue(board);
        if(!isMaxValuePaired(texasHand, board, maxBoardValue)) return false;

        int kickers[] = getBestKickers(board, maxBoardValue);

        for (int i = 0; i < kickers.length; i++){
            for (int j = 2; j < pokerHand.getSize(); j++){
                if(kickers[i] == pokerHand.getCard(j).getValue())  return true;
            }
        }

        if(checkSpecialCase(pokerHand, board))  return true;

        return false;
    }

    @Override
    public SubHandCategory getSubHandCategory() {
        return SubHandCategory.MAX_PAIR_SECOND_KICKER;
    }

    private boolean checkSpecialCase(PokerHand pokerHand, Board board) {
        for (int i = 0; i < pokerHand.getSize(); i++) {
            if(!isOnBoard(pokerHand.getCard(i), board)) return false;
        }
        if(!pokerHand.getCard(0).isAce() && pokerHand.getCard(1).isAce())   return false;
        if(pokerHand.getCard(2).getChar() != 'K')  return false;
        if(pokerHand.getCard(3).getChar() != 'Q')  return false;
        if(pokerHand.getCard(4).getChar() != 'J')  return false;

        return true;
    }

    private boolean isOnBoard(Card card, Board board) {
        for (int i = 0; i < board.size(); i++){
            if(card.isSameCard(board.get(i)))   return true;
        }
        return false;
    }

    private int[] getBestKickers(Board board, int maxBoardValue) {
        int[] kickers = new int[2];
        int value = 14;
        int i = 0;
        boolean kickersIsFill = false;
        while(!kickersIsFill){
            if(isKicker(value, board, maxBoardValue)){
                kickers[i] = value;
                i++;
            }
            value--;
            if(i == 2)  kickersIsFill = true;
        }
        return kickers;
    }

    private boolean isKicker(int value, Board board, int maxBoardValue){
        for (int i = 0; i < board.size(); i++) {
            if(maxBoardValue == value || board.get(i).getValue() == value) return false;
        }
        return true;
    }

    private boolean isMaxValuePaired(TexasHand texasHand, Board board, int maxValue) {
        int occurrences = 0;

        if(texasHand.getCard0().getValue() == maxValue) occurrences++;
        if(texasHand.getCard1().getValue() == maxValue) occurrences++;
        for (int i = 0; i < board.size(); i++) {
            if(board.get(i).getValue() == maxValue) occurrences++;
        }

        return occurrences == 2;
    }

    private int getMaxValue(Board board) {
        int maxValue = board.get(0).getValue();
        for (int i = 1; i < board.size(); i++) {
            if(maxValue < board.get(i).getValue())
                maxValue = board.get(i).getValue();
        }

        return maxValue;
    }
}
