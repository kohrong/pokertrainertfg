package josue.pokertrainer.checkers.subHandCategoryChecker;

import josue.pokertrainer.checkers.handCategoryChecker.HandCategory;
import josue.pokertrainer.checkers.handCategoryChecker.HandCategoryProvider;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.PokerHand;
import josue.pokertrainer.model.TexasHand;
import josue.pokertrainer.utils.PokerHandCalculator;

/**
 * Created by Josue on 25/08/14.
 */
public class MediumPairSecondKickerChecker implements SubHandCategoryChecker {
    @Override
    public boolean check(TexasHand texasHand, Board board) {
        PokerHand pokerHand = new PokerHandCalculator().calculate(texasHand, board);
        if(new HandCategoryProvider().getHandCategory(pokerHand) != HandCategory.ONE_PAIR)   return false;

        int maxValue = getMaxValue(board);
        int lowerValue = getLowerValue(board);

        if(isPaired(texasHand, board, maxValue) || isPaired(texasHand, board, lowerValue))  return false;

        int pairedValue = getPairedValue(board, texasHand);
        int kickers[] = getBestKickers(board, pairedValue);

        return hasKicker(texasHand, kickers);
    }

    @Override
    public SubHandCategory getSubHandCategory() {
        return SubHandCategory.MEDIUM_PAIR_SECOND_KICKER;
    }

    private int getPairedValue(Board board, TexasHand texasHand){
        for (int i = 0; i < board.size(); i++){
            if (isPaired(texasHand, board, board.get(i).getValue()))   return board.get(i).getValue();
        }
        return 0;
    }

    private int getMaxValue(Board board) {
        int maxValue = board.get(0).getValue();
        for (int i = 1; i < board.size(); i++) {
            if(maxValue < board.get(i).getValue())
                maxValue = board.get(i).getValue();
        }

        return maxValue;
    }

    private int getLowerValue(Board board) {
        int lowerValue = board.get(0).getValue();
        for (int i = 1; i < board.size(); i++) {
            if(lowerValue > board.get(i).getValue())
                lowerValue = board.get(i).getValue();
        }

        return lowerValue;
    }

    private boolean isPaired(TexasHand texasHand, Board board, int value) {
        int occurrences = 0;

        if(texasHand.getCard0().getValue() == value) occurrences++;
        if(texasHand.getCard1().getValue() == value) occurrences++;
        for (int i = 0; i < board.size(); i++) {
            if(board.get(i).getValue() == value) occurrences++;
        }

        return occurrences == 2;
    }

    private boolean hasKicker(TexasHand texasHand, int[] kicker) {
        for (Integer value : kicker){
            if(value == texasHand.getCard0().getValue() || value == texasHand.getCard1().getValue())
                return true;
        }
        return false;
    }

    private int[] getBestKickers(Board board, int pairedValue) {
        int[] kickers = new int[2];
        int value = 14;
        int i = 0;
        boolean kickersIsFill = false;
        while(!kickersIsFill){
            if(isKicker(value, board, pairedValue)){
                kickers[i] = value;
                i++;
            }
            value--;
            if(i == 2)  kickersIsFill = true;
        }
        return kickers;
    }

    private boolean isKicker(int value, Board board, int pairedValue) {
        for (int i = 0; i < board.size(); i++) {
            if (pairedValue == value || board.get(i).getValue() == value) return false;
        }
        return true;
    }
}
