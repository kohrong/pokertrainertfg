package josue.pokertrainer.checkers.subHandCategoryChecker;

import josue.pokertrainer.checkers.handCategoryChecker.HandCategory;
import josue.pokertrainer.checkers.handCategoryChecker.HandCategoryProvider;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.PokerHand;
import josue.pokertrainer.model.TexasHand;
import josue.pokertrainer.utils.PokerHandCalculator;

/**
 * Created by Josue on 10/09/14.
 */
public class StraightChecker implements SubHandCategoryChecker {
    @Override
    public boolean check(TexasHand texasHand, Board board) throws Card.Exception {
        PokerHand pokerHand = new PokerHandCalculator().calculate(texasHand, board);
        return new HandCategoryProvider().getHandCategory(pokerHand) == HandCategory.STRAIGHT;
    }

    @Override
    public SubHandCategory getSubHandCategory() {
        return SubHandCategory.STRAIGHT;
    }
}
