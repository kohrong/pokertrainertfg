package josue.pokertrainer.checkers.subHandCategoryChecker;

/**
 * Created by Josue on 11/09/14.
 */
public enum SubHandCategory {
    MAXIMUM_FULL_HOUSE("Full house máximo o mejor"),
    FULL_HOUSE("Full house"),
    BEST_FLUSH("Mejor color"),
    SECOND_FLUSH("Segundo mejor color"),
    FIFTH_FLUSH("Quinto mejor color"),
    FLUSH("Color"),
    TOP_STRAIGHT_TWO_CARDS("Escalera superior con 2 cartas"),
    TOP_STRAIGHT_ONE_CARD("Escalera superior con 1 carta"),
    STRAIGHT("Escalera"),
    THREE_OF_A_KIND("Trío"),
    BEST_TWO_PAIR("Mejores dobles parejas"),
    TWO_PAIR("Dobles parejas"),
    OVERPAIR("Overpair"),
    MAX_PAIR_SECOND_KICKER("Pareja máxima 2º kicker o mejor"),
    MAX_PAIR("Pareja máxima"),
    MEDIUM_PAIR_SECOND_KICKER("Pareja media 2º kicker o mejor"),
    PAIR("Pareja"),
    ACE_HIGH("As alto"),
    NOTHING("Nada");

    private String subCategoryName;

    SubHandCategory(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }
}
