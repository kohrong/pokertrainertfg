package josue.pokertrainer.checkers.subHandCategoryChecker;

import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.TexasHand;

/**
 * Created by Josue on 11/09/14.
 */
public class SubHandCategoryProvider {

    private SubHandCategoryChecker[] subHandCategoryCheckers = new SubHandCategoryChecker[]{new MaximumFullHouseChecker(),
    new FullHouseChecker(), new BestFlushChecker(), new SecondFlushChecker(), new FifthFlushChecker(),
    new FlushChecker(), new TopStraightTwoCardChecker(), new TopStraightOneCardChecker(),
    new StraightChecker(), new ThreeOfAKindChecker(), new BestTwoPairChecker(),
    new TwoPairChecker(), new OverPairChecker(), new MaxPairSecondKickerChecker(),
    new MaxPairChecker(), new MediumPairSecondKickerChecker(), new PairChecker(), new AceHighChecker(),
    new NothingChecker()};

    public SubHandCategory getSubHandCategory(TexasHand texasHand, Board board) throws Card.Exception {
        for (SubHandCategoryChecker subHandCategoryChecker : subHandCategoryCheckers){
            if(subHandCategoryChecker.check(texasHand, board))
                return subHandCategoryChecker.getSubHandCategory();
        }
        return null;
    }

    public int getSubHandCategoryRank(TexasHand texasHand, Board board) throws Card.Exception {
        for (int i = 0; i < subHandCategoryCheckers.length; i++) {
            if(subHandCategoryCheckers[i].check(texasHand, board))  return i;
        }
        return -1;
    }

    public SubHandCategoryChecker[] getSubHandCategoryCheckers(){
        return subHandCategoryCheckers;
    }
}
