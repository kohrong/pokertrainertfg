package josue.pokertrainer.checkers.subHandCategoryChecker;

import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.TexasHand;

/**
 * Created by Josue on 10/09/14.
 */
public class NothingChecker implements SubHandCategoryChecker {
    @Override
    public boolean check(TexasHand texasHand, Board board) throws Card.Exception {
        return true;
    }

    @Override
    public SubHandCategory getSubHandCategory() {
        return SubHandCategory.NOTHING;
    }
}
