package josue.pokertrainer.checkers.subHandCategoryChecker;

import josue.pokertrainer.checkers.handCategoryChecker.HandCategory;
import josue.pokertrainer.checkers.handCategoryChecker.HandCategoryProvider;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.PokerHand;
import josue.pokertrainer.model.TexasHand;
import josue.pokertrainer.utils.PokerHandCalculator;

/**
 * Created by Josue on 25/08/14.
 */
public class BestFlushChecker implements SubHandCategoryChecker {

    @Override
    public boolean check(TexasHand texasHand, Board board) throws Card.Exception {
        PokerHand pokerHand = new PokerHandCalculator().calculate(texasHand, board);
        if(new HandCategoryProvider().getHandCategory(pokerHand) != HandCategory.FLUSH)   return false;

        Card card = getBestKicker(board, pokerHand.getCard(0).getSuit());

        if(!isInPokerHand(card, pokerHand))    return false;
        if(card.isSameCard(texasHand.getCard0()) || card.isSameCard(texasHand.getCard1()))
            return true;

        return false;
    }

    @Override
    public SubHandCategory getSubHandCategory() {
        return SubHandCategory.BEST_FLUSH;
    }

    private boolean isInPokerHand(Card card, PokerHand pokerHand) {
        for (int i = 0; i < pokerHand.getSize(); i++) {
            if(pokerHand.getCard(i).isSameCard(card))   return true;
        }
        return false;
    }

    private Card getBestKicker(Board board, CardSuit suit) throws Card.Exception {
        int value = 14;
        while(value > 1){
            if(isInBoard(value, board, suit)){
                value--;
            }
            else
                break;
        }
        return new Card(value, suit);
    }

    private boolean isInBoard(int value, Board board, CardSuit suit) {
        for (int i = 0; i < board.size(); i++){
            if(value == board.get(i).getValue() && board.get(i).getSuit() == suit)    return true;
        }
        return false;
    }
}
