package josue.pokertrainer.checkers.subHandCategoryChecker;

import josue.pokertrainer.checkers.handCategoryChecker.HandCategory;
import josue.pokertrainer.checkers.handCategoryChecker.HandCategoryProvider;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.PokerHand;
import josue.pokertrainer.model.TexasHand;
import josue.pokertrainer.utils.PokerHandCalculator;

/**
 * Created by Josue on 25/08/14.
 */
public class MaxPairChecker implements SubHandCategoryChecker {
    @Override
    public boolean check(TexasHand texasHand, Board board) {
        PokerHand pokerHand = new PokerHandCalculator().calculate(texasHand, board);
        if(new HandCategoryProvider().getHandCategory(pokerHand) != HandCategory.ONE_PAIR)   return false;

        int maxValue = getMaxValue(board);
        if(!isMaxValuePaired(texasHand, board, maxValue)) return false;

        return true;
    }

    @Override
    public SubHandCategory getSubHandCategory() {
        return SubHandCategory.MAX_PAIR;
    }

    private boolean isMaxValuePaired(TexasHand texasHand, Board board, int maxValue) {
        int occurrences = 0;

        if(texasHand.getCard0().getValue() == maxValue) occurrences++;
        if(texasHand.getCard1().getValue() == maxValue) occurrences++;
        for (int i = 0; i < board.size(); i++) {
            if(board.get(i).getValue() == maxValue) occurrences++;
        }

        return occurrences == 2;
    }

    private int getMaxValue(Board board) {
        int maxValue = board.get(0).getValue();
        for (int i = 1; i < board.size(); i++) {
            if(maxValue < board.get(i).getValue())
                maxValue = board.get(i).getValue();
        }

        return maxValue;
    }
}
