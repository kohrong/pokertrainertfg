package josue.pokertrainer.checkers.subHandCategoryChecker;

import josue.pokertrainer.checkers.handCategoryChecker.HandCategory;
import josue.pokertrainer.checkers.handCategoryChecker.HandCategoryProvider;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.PokerHand;
import josue.pokertrainer.model.TexasHand;
import josue.pokertrainer.utils.PokerHandCalculator;

/**
 * Created by Josue on 25/08/14.
 */
public class ThreeOfAKindChecker implements SubHandCategoryChecker {
    @Override
    public boolean check(TexasHand texasHand, Board board) {
        PokerHand pokerHand = new PokerHandCalculator().calculate(texasHand, board);
        return new HandCategoryProvider().getHandCategory(pokerHand) == HandCategory.THREE_OF_A_KIND;
    }

    @Override
    public SubHandCategory getSubHandCategory() {
        return SubHandCategory.THREE_OF_A_KIND;
    }
}
