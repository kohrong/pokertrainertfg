package josue.pokertrainer.checkers.subHandCategoryChecker;

import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.TexasHand;

/**
 * Created by Josue on 22/08/14.
 */
public interface SubHandCategoryChecker {
    public boolean check(TexasHand texasHand, Board board) throws Card.Exception;
    public SubHandCategory getSubHandCategory();
}
