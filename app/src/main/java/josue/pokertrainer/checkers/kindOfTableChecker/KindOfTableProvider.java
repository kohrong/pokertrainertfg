package josue.pokertrainer.checkers.kindOfTableChecker;

import josue.pokertrainer.model.Board;

/**
 * Created by Josue on 11/09/14.
 */
public class KindOfTableProvider {
    private KindOfTableChecker[] kindOfTableCheckers = new KindOfTableChecker[]{
            new ExtremelyCoordinatedChecker(), new CoordinatedChecker(),
            new HalfCoordinatedChecker(), new DryChecker()};

    public KindOfTable getKindOfTable(Board board){
        for (KindOfTableChecker kindOfTableChecker : kindOfTableCheckers){
            if(kindOfTableChecker.check(board)) return kindOfTableChecker.getKindOfTable();
        }

        return null;
    }
}
