package josue.pokertrainer.checkers.kindOfTableChecker;

import java.util.ArrayList;

import josue.pokertrainer.cardListAttributes.CardsConnected;
import josue.pokertrainer.cardListAttributes.SameSuit;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;

public class ExtremelyCoordinatedChecker implements KindOfTableChecker{
	
	@Override
	public boolean check(Board board) {
		return isExtremelyCoordinated(board);
	}

    @Override
    public KindOfTable getKindOfTable() {
        return KindOfTable.EXTREMELY_COORDINATED;
    }

    public boolean isExtremelyCoordinated(Board board) {
		if(board.isPreFlop() || board.isFlop())	return false;
		
		ArrayList<Card> sequence = new ArrayList();
		for(int i = 0; i < board.size(); i++) sequence.add(board.get(i));

		if(new SameSuit().check(sequence, 4))	return true;
		return new CardsConnected().check(sequence, 4);
	}
}
