package josue.pokertrainer.checkers.kindOfTableChecker;

import josue.pokertrainer.model.Board;

public interface KindOfTableChecker {
	public boolean check(Board board);
    public KindOfTable getKindOfTable();
}
