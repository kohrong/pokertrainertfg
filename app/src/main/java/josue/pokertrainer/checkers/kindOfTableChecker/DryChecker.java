package josue.pokertrainer.checkers.kindOfTableChecker;

import josue.pokertrainer.model.Board;

public class DryChecker implements KindOfTableChecker{

	@Override
	public boolean check(Board board) {
		return isDry(board);
	}

    @Override
    public KindOfTable getKindOfTable() {
        return KindOfTable.DRY;
    }

    public boolean isDry(Board board){
		if(new ExtremelyCoordinatedChecker().isExtremelyCoordinated(board) 
				|| new CoordinatedChecker().isCoordinated(board))	return false;
		return true;
	}

}
