package josue.pokertrainer.checkers.kindOfTableChecker;


import java.util.ArrayList;

import josue.pokertrainer.cardListAttributes.CardsConnected;
import josue.pokertrainer.cardListAttributes.SameSuit;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;

public class CoordinatedChecker implements KindOfTableChecker{

	@Override
	public boolean check(Board board) {
		return isCoordinated(board);
	}

    @Override
    public KindOfTable getKindOfTable() {
        return KindOfTable.COORDINATED;
    }

    public boolean isCoordinated(Board board) {
		if(board.isPreFlop() || new ExtremelyCoordinatedChecker().isExtremelyCoordinated(board)) return false;
		
		ArrayList<Card> sequence = new ArrayList();
		for(int i = 0; i < board.size(); i++) sequence.add(board.get(i));
		
		if(new SameSuit().check(sequence, 3))	return true;
		return new CardsConnected().check(sequence, 3);

	}
}
