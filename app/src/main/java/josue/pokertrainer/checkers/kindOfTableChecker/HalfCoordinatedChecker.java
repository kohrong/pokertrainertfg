package josue.pokertrainer.checkers.kindOfTableChecker;

import java.util.ArrayList;

import josue.pokertrainer.cardListAttributes.MaxGapAllowed;
import josue.pokertrainer.cardListAttributes.SameSuit;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;

public class HalfCoordinatedChecker implements KindOfTableChecker{

	@Override
	public boolean check(Board board) {
		return isHalfCoordinated(board);
	}

    @Override
    public KindOfTable getKindOfTable() {
        return KindOfTable.HALF_COORDINATED;
    }

    public boolean isHalfCoordinated(Board board) {
		if(new ExtremelyCoordinatedChecker().isExtremelyCoordinated(board)
				|| new CoordinatedChecker().isCoordinated(board))	return false;
		if(board.isPreFlop())	return false;
		
		ArrayList<Card> sequence = new ArrayList();
		for(int i = 0; i < board.size(); i++)	sequence.add(board.get(i));
		
		if(new SameSuit().check(sequence, 2))
			return new MaxGapAllowed().check(sequence, 4);
		else	return new MaxGapAllowed().check(sequence, 3);
	}
}
