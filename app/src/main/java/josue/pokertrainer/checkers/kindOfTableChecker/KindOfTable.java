package josue.pokertrainer.checkers.kindOfTableChecker;

/**
 * Created by Josue on 11/09/14.
 */
public enum KindOfTable {
    EXTREMELY_COORDINATED("extremelyCoordinated"),
    COORDINATED("coordinated"),
    HALF_COORDINATED("halfCoordinated"),
    DRY("dry");

    private String kindOfTableName;

    KindOfTable(String kindOfTableName) {
        this.kindOfTableName = kindOfTableName;
    }

    public String getKindOfTableName() {
        return kindOfTableName;
    }
}
