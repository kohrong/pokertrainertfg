package josue.pokertrainer.checkers.subKindOfTableChecker;

import josue.pokertrainer.model.Board;

/**
 * Created by Josue on 11/09/14.
 */
public interface SubKindOfTableChecker {
    public boolean check(Board board);
    public SubKindOfTable getSubKindOfTable();
}
