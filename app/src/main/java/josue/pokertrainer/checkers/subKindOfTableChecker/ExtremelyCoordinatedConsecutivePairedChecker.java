package josue.pokertrainer.checkers.subKindOfTableChecker;

import josue.pokertrainer.cardListAttributes.CardsConnected;
import josue.pokertrainer.cardListAttributes.Pairs;
import josue.pokertrainer.checkers.kindOfTableChecker.ExtremelyCoordinatedChecker;
import josue.pokertrainer.model.Board;

/**
 * Created by Josue on 22/08/14.
 */
public class ExtremelyCoordinatedConsecutivePairedChecker implements SubKindOfTableChecker {

    @Override
    public boolean check(Board board) {
        return new ExtremelyCoordinatedChecker().check(board) &&
                new CardsConnected().check(board.getCards(), 4) &&
                new Pairs().check(board.getCards(), 1);
    }

    @Override
    public SubKindOfTable getSubKindOfTable() {
        return SubKindOfTable.EX_COORDINATED_CONSECUTIVE_PAIRED;
    }

}
