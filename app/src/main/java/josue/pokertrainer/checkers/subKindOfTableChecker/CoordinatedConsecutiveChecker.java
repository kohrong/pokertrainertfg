package josue.pokertrainer.checkers.subKindOfTableChecker;

import josue.pokertrainer.cardListAttributes.CardsConnected;
import josue.pokertrainer.checkers.kindOfTableChecker.CoordinatedChecker;
import josue.pokertrainer.model.Board;

/**
 * Created by Josue on 22/08/14.
 */
public class CoordinatedConsecutiveChecker implements SubKindOfTableChecker {

    @Override
    public boolean check(Board board) {
        return new CoordinatedChecker().check(board) &&
                new CardsConnected().check(board.getCards(), 3);
    }

    @Override
    public SubKindOfTable getSubKindOfTable() {
        return SubKindOfTable.COORDINATED_CONSECUTIVE;
    }
}
