package josue.pokertrainer.checkers.subKindOfTableChecker;

import josue.pokertrainer.cardListAttributes.SameSuit;
import josue.pokertrainer.checkers.kindOfTableChecker.CoordinatedChecker;
import josue.pokertrainer.model.Board;

/**
 * Created by Josue on 22/08/14.
 */
public class CoordinatedSameSuitChecker implements SubKindOfTableChecker {

    @Override
    public boolean check(Board board) {
        return new CoordinatedChecker().check(board) &&
                new SameSuit().check(board.getCards(), 3);
    }

    @Override
    public SubKindOfTable getSubKindOfTable() {
        return SubKindOfTable.COORDINATED_SAME_SUIT;
    }

}
