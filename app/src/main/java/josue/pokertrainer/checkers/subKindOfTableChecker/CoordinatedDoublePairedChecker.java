package josue.pokertrainer.checkers.subKindOfTableChecker;

import josue.pokertrainer.cardListAttributes.Pairs;
import josue.pokertrainer.checkers.kindOfTableChecker.CoordinatedChecker;
import josue.pokertrainer.model.Board;

/**
 * Created by Josue on 22/08/14.
 */
public class CoordinatedDoublePairedChecker implements SubKindOfTableChecker {

    @Override
    public boolean check(Board board) {
        return new CoordinatedChecker().check(board) &&
                new Pairs().check(board.getCards(), 2);
    }

    @Override
    public SubKindOfTable getSubKindOfTable() {
        return SubKindOfTable.COORDINATED_DOUBLE_PAIRED;
    }
}
