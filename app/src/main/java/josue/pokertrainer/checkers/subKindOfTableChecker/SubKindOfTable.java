package josue.pokertrainer.checkers.subKindOfTableChecker;

/**
 * Created by Josue on 11/09/14.
 */
public enum SubKindOfTable {
    EX_COORDINATED_SAME_SUIT_PAIRED("4 del mismo palo emparejada"),
    EX_COORDINATED_SAME_SUIT("4 del mismo palo"),
    EX_COORDINATED_CONSECUTIVE_PAIRED("4 cartas consecutivas emparejada"),
    EX_COORDINATED_CONSECUTIVE("4 cartas consecutivas"),
    COORDINATED_DOUBLE_PAIRED("Coordinada doble emparejada"),
    COORDINATED_PAIRED("Coordinada emparejada"),
    COORDINATED_SAME_SUIT("3 del mismo palo"),
    COORDINATED_CONSECUTIVE("3 cartas consecutivas"),
    DRY_DOUBLE_PAIRED("Seca doble emparejada"),
    DRY_PAIRED("Seca emparejada"),
    DRY_UNPAIRED("Seca sin pareja");

    private String subKindOfTableName;

    SubKindOfTable(String subKindOfTableName) {
        this.subKindOfTableName = subKindOfTableName;
    }

    public String getSubKindOfTableName() {
        return subKindOfTableName;
    }
}
