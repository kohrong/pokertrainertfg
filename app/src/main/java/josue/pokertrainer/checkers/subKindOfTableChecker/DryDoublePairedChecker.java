package josue.pokertrainer.checkers.subKindOfTableChecker;

import josue.pokertrainer.cardListAttributes.Pairs;
import josue.pokertrainer.checkers.kindOfTableChecker.DryChecker;
import josue.pokertrainer.model.Board;

/**
 * Created by Josue on 22/08/14.
 */
public class DryDoublePairedChecker implements SubKindOfTableChecker {

    @Override
    public boolean check(Board board) {
        return new DryChecker().check(board) && new Pairs().check(board.getCards(), 2);
    }

    @Override
    public SubKindOfTable getSubKindOfTable() {
        return SubKindOfTable.DRY_DOUBLE_PAIRED;
    }
}
