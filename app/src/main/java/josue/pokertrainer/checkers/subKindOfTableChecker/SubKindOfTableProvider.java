package josue.pokertrainer.checkers.subKindOfTableChecker;

import josue.pokertrainer.model.Board;

public class SubKindOfTableProvider {

    public static final SubKindOfTableChecker[] subKindOfTablesCheckers = new SubKindOfTableChecker[]{
        new ExtremelyCoordinatedSameSuitPairedChecker(),
        new ExtremelyCoordinatedSameSuitChecker(), new ExtremelyCoordinatedConsecutivePairedChecker(),
        new ExtremelyCoordinatedConsecutiveChecker(), new CoordinatedDoublePairedChecker(),
        new CoordinatedPairedChecker(), new CoordinatedSameSuitChecker(),
        new CoordinatedConsecutiveChecker(), new DryDoublePairedChecker(), new DryPairedChecker(),
        new DryUnpairedChecker()
    };

    public SubKindOfTable getSubKindOfTable(Board board){
        for (SubKindOfTableChecker subKindOfTableChecker : subKindOfTablesCheckers){
            if (subKindOfTableChecker.check(board))
                return subKindOfTableChecker.getSubKindOfTable();
        }

        return null;
    }

    public int getSubKindOfTableRank(Board board){
        for (int i = 0; i < subKindOfTablesCheckers.length; i++) {
            if(subKindOfTablesCheckers[i].check(board)) return i;
        }
        return -1;
    }

    public SubKindOfTableChecker[] getSubKindOfTablesCheckers() {
        return subKindOfTablesCheckers;
    }
}
