package josue.pokertrainer.checkers.subKindOfTableChecker;

import josue.pokertrainer.cardListAttributes.SameSuit;
import josue.pokertrainer.checkers.kindOfTableChecker.ExtremelyCoordinatedChecker;
import josue.pokertrainer.model.Board;

/**
 * Created by Josue on 22/08/14.
 */
public class ExtremelyCoordinatedSameSuitChecker implements SubKindOfTableChecker {

    @Override
    public boolean check(Board board) {
        return new ExtremelyCoordinatedChecker().check(board) &&
                new SameSuit().check(board.getCards(), 4);
    }

    @Override
    public SubKindOfTable getSubKindOfTable() {
        return SubKindOfTable.EX_COORDINATED_SAME_SUIT;
    }

}
