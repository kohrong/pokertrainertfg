package josue.pokertrainer.control.indicatorCreators;

import josue.pokertrainer.checkers.handStrengthChecker.HandStrength;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.Player;
import josue.pokertrainer.model.Table;
import josue.pokertrainer.model.indicators.HandStrengthIndicator;
import josue.pokertrainer.model.indicators.Indicator;

/**
 * Created by Josue on 15/09/14.
 */
public class HandStrengthIndicatorCreator implements IndicatorCreator {
    @Override
    public Indicator create(Player player, Table table) throws Card.Exception {
        HandStrength.Strength handStrength = new HandStrength().getStrength(player.getTexasHand(), table.getBoard());

        return new HandStrengthIndicator(handStrength.toString());
    }
}
