package josue.pokertrainer.control.indicatorCreators;

import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.Player;
import josue.pokertrainer.model.Table;
import josue.pokertrainer.model.indicators.Indicator;

/**
 * Created by Josue on 15/09/14.
 */
public interface IndicatorCreator {
    public Indicator create(Player player, Table table) throws Card.Exception;
}
