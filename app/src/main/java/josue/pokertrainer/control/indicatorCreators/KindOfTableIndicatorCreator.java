package josue.pokertrainer.control.indicatorCreators;

import josue.pokertrainer.checkers.kindOfTableChecker.KindOfTable;
import josue.pokertrainer.checkers.kindOfTableChecker.KindOfTableProvider;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.Player;
import josue.pokertrainer.model.Table;
import josue.pokertrainer.model.indicators.Indicator;
import josue.pokertrainer.model.indicators.KindOfTableIndicator;

/**
 * Created by Josue on 15/09/14.
 */
public class KindOfTableIndicatorCreator implements IndicatorCreator {
    @Override
    public Indicator create(Player player, Table table) throws Card.Exception {
        KindOfTable kindOfTable = new KindOfTableProvider().getKindOfTable(table.getBoard());

        return new KindOfTableIndicator(kindOfTable.getKindOfTableName());
    }
}
