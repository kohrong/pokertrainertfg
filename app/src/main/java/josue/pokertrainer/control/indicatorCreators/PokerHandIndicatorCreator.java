package josue.pokertrainer.control.indicatorCreators;

import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.Player;
import josue.pokertrainer.model.PokerHand;
import josue.pokertrainer.model.Table;
import josue.pokertrainer.model.indicators.Indicator;
import josue.pokertrainer.model.indicators.PokerHandIndicator;
import josue.pokertrainer.utils.PokerHandCalculator;

/**
 * Created by Josue on 15/09/14.
 */
public class PokerHandIndicatorCreator implements IndicatorCreator {
    @Override
    public Indicator create(Player player, Table table) throws Card.Exception {
        PokerHand pokerHand = new PokerHandCalculator().calculate(player.getTexasHand(), table.getBoard());

        String pokerHandName = new String();
        for (int i = 0; i < pokerHand.getSize(); i++) {
            pokerHandName = pokerHandName.concat((String.valueOf(pokerHand.getCard(i).getChar())));
        }

        return new PokerHandIndicator(pokerHandName);
    }
}
