package josue.pokertrainer.control.indicatorCreators;

import josue.pokertrainer.checkers.handCategoryChecker.HandCategory;
import josue.pokertrainer.checkers.handCategoryChecker.HandCategoryProvider;
import josue.pokertrainer.model.Player;
import josue.pokertrainer.model.PokerHand;
import josue.pokertrainer.model.Table;
import josue.pokertrainer.model.indicators.HandCategoryIndicator;
import josue.pokertrainer.model.indicators.Indicator;
import josue.pokertrainer.utils.PokerHandCalculator;

/**
 * Created by Josue on 15/09/14.
 */
public class HandCategoryIndicatorCreator implements IndicatorCreator {
    @Override
    public Indicator create(Player player, Table table) {
        PokerHand pokerHand = new PokerHandCalculator().calculate(player.getTexasHand(), table.getBoard());
        HandCategory handCategory = new HandCategoryProvider().getHandCategory(pokerHand);

        return new HandCategoryIndicator(handCategory.getCategoryName());
    }
}
