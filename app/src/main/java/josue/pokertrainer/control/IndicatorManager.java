package josue.pokertrainer.control;

import java.util.ArrayList;

import josue.pokertrainer.control.indicatorCreators.HandCategoryIndicatorCreator;
import josue.pokertrainer.control.indicatorCreators.HandStrengthIndicatorCreator;
import josue.pokertrainer.control.indicatorCreators.IndicatorCreator;
import josue.pokertrainer.control.indicatorCreators.KindOfTableIndicatorCreator;
import josue.pokertrainer.control.indicatorCreators.PokerHandIndicatorCreator;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.Player;
import josue.pokertrainer.model.Table;
import josue.pokertrainer.model.indicators.Indicator;

/**
 * Created by Josue on 15/09/14.
 */
public class IndicatorManager {
    private IndicatorCreator[] indicatorCreators = new IndicatorCreator[]{
            new HandCategoryIndicatorCreator(), new KindOfTableIndicatorCreator(),
            new HandStrengthIndicatorCreator(), new PokerHandIndicatorCreator()
    };

    public ArrayList<Indicator> getIndicators(Player player, Table table) throws Card.Exception {
        ArrayList<Indicator> indicators = new ArrayList<>();

        for (IndicatorCreator indicatorCreator: indicatorCreators){
            indicators.add(indicatorCreator.create(player, table));
        }

        return indicators;
    }

    public Indicator getIndicator(int i, Player player, Table table) throws Card.Exception {
        return indicatorCreators[i].create(player, table);
    }
}
