package josue.pokertrainer.control.sceneCreators;

import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.Deck;
import josue.pokertrainer.model.Player;
import josue.pokertrainer.model.Scene;
import josue.pokertrainer.model.Table;
import josue.pokertrainer.model.TexasHand;

/**
 * Created by Josue on 15/09/14.
 */
public class SceneCreator {
    private Deck deck;

    public SceneCreator() throws Card.Exception {
        deck = new Deck();
    }

    public Scene createScene() throws Card.Exception{
        Player player = new Player("Hero", new TexasHand(deck.takeRandomCard(), deck.takeRandomCard()));

        Table table = new Table(new Board(new Card[]{deck.takeRandomCard(), deck.takeRandomCard(), deck.takeRandomCard()}), deck);

        return new Scene(player, table);
    }
}
