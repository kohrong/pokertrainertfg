package josue.pokertrainer.utils;

import java.util.ArrayList;

import josue.pokertrainer.cardListAttributes.CardsConnected;
import josue.pokertrainer.checkers.handCategoryChecker.HandCategory;
import josue.pokertrainer.checkers.handCategoryChecker.HandCategoryProvider;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.TexasHand;

public class StraightProject {
	private Board board;
	private TexasHand texasHand;
	private PokerHandCalculator pokerHandCalculator;
	
	public StraightProject(Board board, TexasHand texasHand) {
		super();
		this.board = board;
		this.texasHand = texasHand;
		pokerHandCalculator = new PokerHandCalculator();
	}
	
	public boolean isStraightProject(){
		if(board.isRiver() || board.isPreFlop())
			return false;
		else if(new HandCategoryProvider().getHandCategory(pokerHandCalculator.calculate(texasHand, board))
                == HandCategory.STRAIGHT)
			return false;
		else
			return new CardsConnected().check(createSequence(), 4);
	}
	
	private ArrayList<Card> createSequence() {
		ArrayList<Card> sequence = new ArrayList();
		sequence.add(texasHand.getCard0());
		sequence.add(texasHand.getCard1());
		for(int i = 0; i < board.size(); i++)	sequence.add(board.get(i));
		return sequence;
	}
}
