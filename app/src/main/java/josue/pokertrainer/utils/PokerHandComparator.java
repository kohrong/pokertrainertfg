package josue.pokertrainer.utils;

import josue.pokertrainer.checkers.handCategoryChecker.HandCategoryProvider;
import josue.pokertrainer.model.PokerHand;

public class PokerHandComparator {

    public int compareTo(PokerHand o1, PokerHand o2) {
        int returnValue = compareByCategory(o1, o2);
        if(returnValue != 0) return returnValue;
        return compareByCardRank(o1, o2);
    }

    private int compareByCategory(PokerHand o1, PokerHand o2) {
        if (new HandCategoryProvider().getHandCategoryRank(o1) > new HandCategoryProvider().getHandCategoryRank(o2))
            return -1;
        else if (new HandCategoryProvider().getHandCategory(o1) == new HandCategoryProvider().getHandCategory(o2))
            return 0;
        else
            return 1;
    }

    private int compareByCardRank(PokerHand o1, PokerHand o2) {
        for (int i = 0; i < o1.getSize(); i++)
            if (o1.getCard(i).getValue() > o2.getCard(i).getValue())
                return -1;
            else if (o1.getCard(i).getValue() < o2.getCard(i).getValue())
                return 1;
        return 0;
    }
}
