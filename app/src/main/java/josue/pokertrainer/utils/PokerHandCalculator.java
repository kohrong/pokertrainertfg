package josue.pokertrainer.utils;

import java.util.ArrayList;

import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.PokerHand;
import josue.pokertrainer.model.TexasHand;

public class PokerHandCalculator {

	private ArrayList<PokerHand> pokerHandList = new ArrayList<PokerHand>();

	public PokerHand calculate(TexasHand texasHand, Board board) {
		if(board.isPreFlop())	return null;

		ArrayList<Card> sequence = board.getCards();
        sequence.add(texasHand.getCard0());
        sequence.add(texasHand.getCard1());

		if(board.isFlop())
			boardOnFlop(sequence);
		if (board.isTurn()){
			boardOnTurn(sequence);
		}
		if (board.isRiver()) {
			boardOnRiver(sequence);
		}

        PokerHand pokerHand = comparePokerHands();
        return pokerHand;
	}

	private PokerHand comparePokerHands() {
		PokerHand pokerHand = pokerHandList.get(0);
		for (int i = 1; i < pokerHandList.size(); i++) {
			if(new PokerHandComparator().compareTo(pokerHand, pokerHandList.get(i)) == 1){
				pokerHand = pokerHandList.get(i);
			}
				
		}
		return pokerHand;
	}

	private void boardOnRiver(ArrayList<Card> sequence) {
		Card[] cards = new Card[5];
		
		for (int i = 0; i < (sequence.size())-1; i++) {
			for (int j = i+1; j < sequence.size(); j++) {
				int index = 0;
				for (int k = 0; k < sequence.size(); k++) {
					if(k != i && k != j)
						cards[index++] = sequence.get(k);
				}
				pokerHandList.add(new PokerHand(cards));
			}
		}
		
	}

	private void boardOnTurn(ArrayList<Card> sequence) {
		Card[] cards = new Card[5];
		
		for (int i = 0; i < sequence.size(); i++) {
			int index = 0;
			for(int j = 0; j < sequence.size(); j++){
				if(j != i)
					cards[index++] = sequence.get(j);
			}
			pokerHandList.add(new PokerHand(cards));
		}
	}

	private void boardOnFlop(ArrayList<Card> sequence) {
		pokerHandList.add(new PokerHand(sequence.toArray(new Card[sequence.size()])));
	}

}
