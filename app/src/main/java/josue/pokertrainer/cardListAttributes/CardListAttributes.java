package josue.pokertrainer.cardListAttributes;

import java.util.ArrayList;

import josue.pokertrainer.model.Card;

public interface CardListAttributes {
	public abstract boolean check(ArrayList<Card> sequence, int target);
}
