package josue.pokertrainer.cardListAttributes;

import java.util.ArrayList;

import josue.pokertrainer.model.Card;

/**
 * Created by Josue on 20/08/14.
 */
public class ConsecutiveCards implements CardListAttributes {

    @Override
    public boolean check(ArrayList<Card> sequence, int target) {
        return consecutiveCards(sequence, target);
    }

    private boolean consecutiveCards(ArrayList<Card> sequence, int target) {
        int maxConsecutive = 0;
        int currentConsecutive;

        for(int i = 0; i < sequence.size()-1; i++){
            currentConsecutive = isNextCard(sequence.get(i).getNumber(), sequence);
            if(currentConsecutive > maxConsecutive)
                maxConsecutive = currentConsecutive;

        }

        return maxConsecutive == target;
    }

    private int isNextCard(int number, ArrayList<Card> sequence) {
        for (int i = 0; i < sequence.size(); i++){
            if(number == 13){
                if (sequence.get(i).getNumber() == 1)   return 2;
            }
            else if(sequence.get(i).getNumber() - number == 1){
                return 1 + isNextCard(number + 1, sequence);
            }
        }
        return 0;
    }
}
