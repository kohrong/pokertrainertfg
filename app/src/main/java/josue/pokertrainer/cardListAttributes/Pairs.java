package josue.pokertrainer.cardListAttributes;

import java.util.ArrayList;
import java.util.List;

import josue.pokertrainer.model.Card;

/**
 * Created by Josue on 19/08/14.
 */
public class Pairs implements CardListAttributes {

    @Override
    public boolean check(ArrayList<Card> sequence, int target) {
        return cardsPaired(sequence, target);
    }

    private boolean cardsPaired(ArrayList<Card> sequence, int target) {
        int pairs = 0;

        for (int i = 0; i < sequence.size() - 1; i++){
            if(hasOnePair(sequence.get(i), sequence.subList(i + 1, sequence.size()))) {
                pairs++;
            }
        }
        return pairs == target;
    }

    private boolean hasOnePair(Card card, List<Card> cards) {
        int sameNumber = 0;
        for (int i = 0; i < cards.size(); i++){
            if (card.isSameNumber(cards.get(i))){
                sameNumber++;
            }
        }
        return sameNumber == 1;
    }
}
