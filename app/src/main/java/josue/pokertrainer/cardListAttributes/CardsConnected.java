package josue.pokertrainer.cardListAttributes;

import java.util.ArrayList;

import josue.pokertrainer.model.Card;

public class CardsConnected implements CardListAttributes {
	
	public boolean check(ArrayList<Card> sequence, int connectedTarget){
		return cardsConnected(sequence, connectedTarget);
	}
	
	private boolean cardsConnected(ArrayList<Card> sequence, int connectedTarget){
		if((sequence.size() - connectedTarget) == 0)	return straightProjectWithoutHollow(sequence);
		else if((sequence.size() - connectedTarget) == 1)	return straightProjectWithOneHollow(sequence);
		return straightProjectWithTwoHollows(sequence);
	}

	private boolean straightProjectWithoutHollow(ArrayList<Card> sequence) {
		return isSubSequenceConnected(sequence);
	}
	
	private boolean straightProjectWithOneHollow(ArrayList<Card> sequence){
		ArrayList<Card> subSequence = new ArrayList<Card>();
		boolean isProject = false;
 		
		for(int i = 0; i < sequence.size(); i++){
			for(int j = 0; j < sequence.size(); j++){
				if(j != i)	subSequence.add(sequence.get(j));
			}
			isProject = isSubSequenceConnected(subSequence);
			if(isProject) return true;
			subSequence.clear();
		}
		return false;
	}
	
	private boolean straightProjectWithTwoHollows(ArrayList<Card> sequence) {
		ArrayList<Card> subSequence = new ArrayList<Card>();
		boolean isProject = false;
 		
		for(int i = 0; i < sequence.size()-1; i++){
			for(int j = i+1; j < sequence.size(); j++){
				for(int k=0; k < sequence.size(); k++){
					if(k != i && k != j)
						subSequence.add(sequence.get(k));
				}
				isProject = isSubSequenceConnected(subSequence);
				if(isProject) return true;
				subSequence.clear();
			}			
		}
		return false;
	}
	
	private boolean isSubSequenceConnected(ArrayList<Card> subSequence) {
		for(int i = 0; i < subSequence.size()-1; i++){
			for(int j = i+1; j < subSequence.size(); j++){
				if(!subSequence.get(i).isConnected(subSequence.get(j)))
					return false;
			}
		}
		return true;
	}
}
