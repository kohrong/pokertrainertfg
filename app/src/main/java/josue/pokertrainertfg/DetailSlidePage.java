package josue.pokertrainertfg;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import josue.pokertrainertfg.utils.TextViewEx;

/**
 * Created by Josue on 14/10/14.
 */
public class DetailSlidePage extends Fragment {

    private static final String CARD0 = "card0";
    private static final String CARD1 = "card1";
    private static final String CARD2 = "card2";
    private static final String CARD3 = "card3";
    private static final String CARD4 = "card4";

    private static final String TITLE = "title";
    private static final String DETAIL = "detail";
    private static final String INDEX = "index";

    private Integer[] imageResources = new Integer[5];
    private Integer titleResource;
    private Integer stringResource;
    private Integer index;

    public static DetailSlidePage newInstance(LessonDetail lessonDetail, int index){
        DetailSlidePage fragment = new DetailSlidePage();

        Bundle bundle = new Bundle();
        bundle.putInt(CARD0, lessonDetail.getImageResources()[0]);
        bundle.putInt(CARD1, lessonDetail.getImageResources()[1]);
        bundle.putInt(CARD2, lessonDetail.getImageResources()[2]);
        bundle.putInt(CARD3, lessonDetail.getImageResources()[3]);
        bundle.putInt(CARD4, lessonDetail.getImageResources()[4]);
        bundle.putInt(TITLE, lessonDetail.getTitleResource());
        bundle.putInt(DETAIL, lessonDetail.getStringResource());
        bundle.putInt(INDEX, index);

        fragment.setArguments(bundle);
        fragment.setRetainInstance(true);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Load parameters when the initial creation of the fragment is done
        this.imageResources[0] = (getArguments() != null) ? getArguments().getInt(
                CARD0) : R.drawable.back;
        this.imageResources[1] = (getArguments() != null) ? getArguments().getInt(
                CARD1) : R.drawable.back;
        this.imageResources[2] = (getArguments() != null) ? getArguments().getInt(
                CARD2) : R.drawable.back;
        this.imageResources[3] = (getArguments() != null) ? getArguments().getInt(
                CARD3) : R.drawable.back;
        this.imageResources[4] = (getArguments() != null) ? getArguments().getInt(
                CARD4) : R.drawable.back;
        this.titleResource = (getArguments() != null) ? getArguments().getInt(
                TITLE) : -1;
        this.stringResource = (getArguments() != null) ? getArguments().getInt(
                DETAIL) : -1;
        this.index = (getArguments() != null) ? getArguments().getInt(INDEX)
                : -1;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.lesson_detail_slide_page, container, false);

        setTitle(rootView);
        setCards(rootView);
        setLessonDetail(rootView);

        return rootView;
    }

    private void setLessonDetail(ViewGroup rootView) {
        TextViewEx textViewEx = (TextViewEx) rootView.findViewById(R.id.lessonDetail);
        textViewEx.setText(getText(stringResource));
    }

    private void setCards(ViewGroup rootView) {
        ImageView[] imageViews = new ImageView[]{(ImageView) rootView.findViewById(R.id.card0), (ImageView) rootView.findViewById(R.id.card1),
                (ImageView) rootView.findViewById(R.id.card2), (ImageView) rootView.findViewById(R.id.card3), (ImageView) rootView.findViewById(R.id.card4)};

        for (int i = 0; i < 5; i++){
            imageViews[i].setImageResource(imageResources[i]);
        }
    }

    private void setTitle(ViewGroup rootView) {
        TextView title = (TextView) rootView.findViewById(R.id.lessonTitle);
        title.setText(getText(titleResource));
    }
}
