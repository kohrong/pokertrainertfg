package josue.pokertrainertfg;

import java.util.ArrayList;

/**
 * Created by Josue on 05/10/14.
 */
public class Quiz {

    private ArrayList<String> possibleAnswerList;
    private String rightAnswer;

    public Quiz(String[] possibleAnswers, String rightAnswer) {
        possibleAnswerList = new ArrayList<>();
        for (int i = 0; i < possibleAnswers.length; i++){
            possibleAnswerList.add(possibleAnswers[i]);
        }
        this.rightAnswer = rightAnswer;
    }

    public ArrayList<String> getRandomQuiz(int size){
        ArrayList<String> quizList = new ArrayList<>();
        quizList.add(rightAnswer);
        possibleAnswerList.remove(rightAnswer);

        for (int i = 0; i < size - 1; i++){
            int index = new Double(Math.random() * 100).intValue() % possibleAnswerList.size();
            String answer = possibleAnswerList.get(index);

            quizList.add(answer);
            possibleAnswerList.remove(answer);
        }

        return quizList;
    }
}
