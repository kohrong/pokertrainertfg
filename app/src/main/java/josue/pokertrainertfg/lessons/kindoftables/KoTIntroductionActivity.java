package josue.pokertrainertfg.lessons.kindoftables;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import josue.pokertrainertfg.LessonDetail;
import josue.pokertrainertfg.R;
import josue.pokertrainertfg.fillers.LessonIntroductionFiller;
import josue.pokertrainertfg.lessons.DetailActivity;
import josue.pokertrainertfg.lessons.detailcreators.KoTDetailCreator;
import josue.pokertrainertfg.lessons.detailcreators.StreetDetailCreator;

/**
 * Created by Josue on 06/10/14.
 */
public class KoTIntroductionActivity extends Activity{

    private SharedPreferences sharedPreferences;

    private static final String KOT_DATA = ("kotData");
    private static final String UNLOCK_GO_BUTTON = ("unlockGoButton");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lesson_introduction);

        sharedPreferences = getSharedPreferences(KOT_DATA, MODE_PRIVATE);

        LessonIntroductionFiller lessonIntroductionFiller = new LessonIntroductionFiller(R.string.title_section4, R.string.kotLessonResume);
        lessonIntroductionFiller.fillView(findViewById(R.id.lessonIntroduction));

        ImageView learnImage = (ImageView) findViewById(R.id.learnImage);
        learnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LessonDetail[] lessonDetails = new KoTDetailCreator().create();
                Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
                intent.putExtra("detail", lessonDetails);
                intent.putExtra("data", KOT_DATA);
                startActivity(intent);
            }
        });

        ImageView goImage = (ImageView) findViewById(R.id.goImage);
        setGoImage(goImage);
        goImage.setEnabled(sharedPreferences.getBoolean(UNLOCK_GO_BUTTON, false));
        goImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), KoTTableActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setGoImage(ImageView goImage) {
        if (!sharedPreferences.getBoolean(UNLOCK_GO_BUTTON, false)) goImage.setImageResource(R.drawable.ic_gobutton_locked);
        else goImage.setImageResource(R.drawable.ic_go);
    }

    @Override
    protected void onResume(){
        super.onResume();
        ImageView goImage = (ImageView) findViewById(R.id.goImage);
        goImage.setEnabled(sharedPreferences.getBoolean(UNLOCK_GO_BUTTON, false));
        setGoImage(goImage);

        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        // Remember that you should never show the action bar if the
        // status bar is hidden, so hide that too if necessary.
        ActionBar actionBar = getActionBar();
        actionBar.hide();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.lesson1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
