package josue.pokertrainertfg.lessons.handstrength;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import josue.pokertrainertfg.LessonDetail;
import josue.pokertrainertfg.R;
import josue.pokertrainertfg.fillers.LessonIntroductionFiller;
import josue.pokertrainertfg.lessons.DetailActivity;
import josue.pokertrainertfg.lessons.detailcreators.HandStrengthDetailCreator;
import josue.pokertrainertfg.lessons.detailcreators.StreetDetailCreator;

/**
 * Created by Josue on 08/10/14.
 */
public class HandStrengthIntroductionActivity extends Activity {

    private SharedPreferences sharedPreferences;
    private static final String STRENGTH_DATA = ("strengthData");
    private static final String UNLOCK_GO_BUTTON = ("unlockGoButton");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lesson_introduction);

        sharedPreferences = getSharedPreferences(STRENGTH_DATA, MODE_PRIVATE);

        LessonIntroductionFiller lessonIntroductionFiller = new LessonIntroductionFiller(R.string.title_section5, R.string.handStrengthInfo);
        lessonIntroductionFiller.fillView(findViewById(R.id.lessonIntroduction));

        ImageView learnImage = (ImageView) findViewById(R.id.learnImage);
        learnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LessonDetail[] lessonDetails = new HandStrengthDetailCreator().create();
                Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
                intent.putExtra("detail", lessonDetails);
                intent.putExtra("data", STRENGTH_DATA);
                startActivity(intent);
            }
        });

        ImageView goImage = (ImageView) findViewById(R.id.goImage);
        setGoImage(goImage);
        goImage.setEnabled(sharedPreferences.getBoolean(UNLOCK_GO_BUTTON, false));
        goImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), HandStrengthTableActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setGoImage(ImageView goImage) {
        if (!sharedPreferences.getBoolean(UNLOCK_GO_BUTTON, false)) goImage.setImageResource(R.drawable.ic_gobutton_locked);
        else goImage.setImageResource(R.drawable.ic_go);
    }

    @Override
    protected void onResume(){
        super.onResume();
        ImageView goImage = (ImageView) findViewById(R.id.goImage);
        goImage.setEnabled(sharedPreferences.getBoolean(UNLOCK_GO_BUTTON, false));
        setGoImage(goImage);

        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        // Remember that you should never show the action bar if the
        // status bar is hidden, so hide that too if necessary.
        ActionBar actionBar = getActionBar();
        actionBar.hide();
    }
}
