package josue.pokertrainertfg.lessons.detailcreators;

import josue.pokertrainertfg.LessonDetail;
import josue.pokertrainertfg.R;

/**
 * Created by Josue on 14/10/14.
 */
public class StreetDetailCreator implements DetailCreator {
    @Override
    public LessonDetail[] create() {
        LessonDetail[] lessonDetails = new LessonDetail[4];
        lessonDetails[0] = new LessonDetail(new int[]{R.drawable.back, R.drawable.back, R.drawable.back, R.drawable.back, R.drawable.back},
                R.string.preflop, R.string.preflopDetail);
        lessonDetails[1] = new LessonDetail(new int[]{R.drawable.s8, R.drawable.h8, R.drawable.hq, R.drawable.back, R.drawable.back},
                R.string.flop, R.string.flopDetail);
        lessonDetails[2] = new LessonDetail(new int[]{R.drawable.sq, R.drawable.hq, R.drawable.s8, R.drawable.h8, R.drawable.back},
                R.string.turn, R.string.turnDetail);
        lessonDetails[3] = new LessonDetail(new int[]{R.drawable.dj, R.drawable.sj, R.drawable.hj, R.drawable.c3, R.drawable.s2},
                R.string.river, R.string.riverDetail);

        return lessonDetails;
    }
}
