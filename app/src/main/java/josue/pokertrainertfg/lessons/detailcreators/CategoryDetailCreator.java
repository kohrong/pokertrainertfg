package josue.pokertrainertfg.lessons.detailcreators;

import josue.pokertrainertfg.LessonDetail;
import josue.pokertrainertfg.R;

/**
 * Created by Josue on 14/10/14.
 */
public class CategoryDetailCreator implements DetailCreator {
    @Override
    public LessonDetail[] create() {
        LessonDetail[] lessonDetails = new LessonDetail[9];
        lessonDetails[0] = new LessonDetail(new int[]{R.drawable.sa, R.drawable.s6, R.drawable.d5, R.drawable.c3, R.drawable.s2},
                R.string.highCard, R.string.highCardDetail);
        lessonDetails[1] = new LessonDetail(new int[]{R.drawable.s8, R.drawable.h8, R.drawable.hq, R.drawable.s5, R.drawable.s2},
                R.string.onePair, R.string.onePairDetail);
        lessonDetails[2] = new LessonDetail(new int[]{R.drawable.sq, R.drawable.hq, R.drawable.s8, R.drawable.h8, R.drawable.s2},
                R.string.twoPair, R.string.twoPairDetail);
        lessonDetails[3] = new LessonDetail(new int[]{R.drawable.dj, R.drawable.sj, R.drawable.hj, R.drawable.c3, R.drawable.s2},
                R.string.threeOfAKind, R.string.threeOfAKindDetail);
        lessonDetails[4] = new LessonDetail(new int[]{R.drawable.s6, R.drawable.d5, R.drawable.h4, R.drawable.c3, R.drawable.s2},
                R.string.straight, R.string.straightDetail);
        lessonDetails[5] = new LessonDetail(new int[]{R.drawable.sa, R.drawable.s6, R.drawable.s5, R.drawable.s3, R.drawable.s2},
                R.string.flush, R.string.flushDetail);
        lessonDetails[6] = new LessonDetail(new int[]{R.drawable.sa, R.drawable.da, R.drawable.ha, R.drawable.h2, R.drawable.s2},
                R.string.fullHouse, R.string.fullHouseDetail);
        lessonDetails[7] = new LessonDetail(new int[]{R.drawable.sa, R.drawable.ha, R.drawable.ca, R.drawable.da, R.drawable.s2},
                R.string.poker, R.string.pokerDetail);
        lessonDetails[8] = new LessonDetail(new int[]{R.drawable.sa, R.drawable.sk, R.drawable.sq, R.drawable.sj, R.drawable.st},
                R.string.straightFlush, R.string.straightFlushDetail);

        return lessonDetails;
    }
}
