package josue.pokertrainertfg.lessons.detailcreators;

import josue.pokertrainertfg.LessonDetail;
import josue.pokertrainertfg.R;

/**
 * Created by Josue on 14/10/14.
 */
public class ProjectDetailCreator implements DetailCreator {
    @Override
    public LessonDetail[] create() {
        LessonDetail[] lessonDetails = new LessonDetail[2];
        lessonDetails[0] = new LessonDetail(new int[]{R.drawable.sa, R.drawable.d4, R.drawable.s3, R.drawable.s6, R.drawable.sj},
                R.string.flushProject, R.string.flushProjectDetail);
        lessonDetails[1] = new LessonDetail(new int[]{R.drawable.d7, R.drawable.d8, R.drawable.h9, R.drawable.c6, R.drawable.sk},
                R.string.straightProject, R.string.straightProjectDetail);

        return lessonDetails;
    }
}
