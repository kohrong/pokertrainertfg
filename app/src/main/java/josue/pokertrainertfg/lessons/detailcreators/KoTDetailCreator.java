package josue.pokertrainertfg.lessons.detailcreators;

import josue.pokertrainertfg.LessonDetail;
import josue.pokertrainertfg.R;

/**
 * Created by Josue on 14/10/14.
 */
public class KoTDetailCreator implements DetailCreator {
    @Override
    public LessonDetail[] create() {
        LessonDetail[] lessonDetails = new LessonDetail[4];
        lessonDetails[0] = new LessonDetail(new int[]{R.drawable.s2, R.drawable.d7, R.drawable.hk, R.drawable.back, R.drawable.back},
                R.string.dry, R.string.dryDetail);
        lessonDetails[1] = new LessonDetail(new int[]{R.drawable.sj, R.drawable.s5, R.drawable.d7, R.drawable.back, R.drawable.back},
                R.string.halfCoordinated, R.string.halfCoordinatedDetail);
        lessonDetails[2] = new LessonDetail(new int[]{R.drawable.sj, R.drawable.h8, R.drawable.d7, R.drawable.back, R.drawable.back},
                R.string.coordinated, R.string.coordinatedDetail);
        lessonDetails[3] = new LessonDetail(new int[]{R.drawable.s6, R.drawable.sj, R.drawable.s8, R.drawable.s5, R.drawable.back},
                R.string.extremelyCoordinated, R.string.extremelyCoordinatedDetail);

        return lessonDetails;
    }
}
