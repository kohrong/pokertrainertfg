package josue.pokertrainertfg.lessons.detailcreators;

import josue.pokertrainertfg.LessonDetail;

/**
 * Created by Josue on 14/10/14.
 */
public interface DetailCreator {

    public LessonDetail[] create();
}
