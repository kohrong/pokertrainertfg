package josue.pokertrainertfg.lessons.detailcreators;

import josue.pokertrainertfg.LessonDetail;
import josue.pokertrainertfg.R;

/**
 * Created by Josue on 14/10/14.
 */
public class HandStrengthDetailCreator implements DetailCreator {
    @Override
    public LessonDetail[] create() {
        LessonDetail[] lessonDetails = new LessonDetail[5];
        lessonDetails[0] = new LessonDetail(new int[]{R.drawable.dk, R.drawable.dt, R.drawable.d6, R.drawable.d7, R.drawable.h8},
                R.string.nothing, R.string.nothingDetail);
        lessonDetails[1] = new LessonDetail(new int[]{R.drawable.sa, R.drawable.dk, R.drawable.cq, R.drawable.d9, R.drawable.h8},
                R.string.weak, R.string.weakDetail);
        lessonDetails[2] = new LessonDetail(new int[]{R.drawable.sa, R.drawable.da, R.drawable.sq, R.drawable.h8, R.drawable.ht},
                R.string.medium, R.string.mediumDetail);
        lessonDetails[3] = new LessonDetail(new int[]{R.drawable.dj, R.drawable.sj, R.drawable.s9, R.drawable.h9, R.drawable.h8},
                R.string.strong, R.string.strongDetail);
        lessonDetails[4] = new LessonDetail(new int[]{R.drawable.sj, R.drawable.ht, R.drawable.h9, R.drawable.s8, R.drawable.c7},
                R.string.veryStrong, R.string.veryStrongDetail);
        return lessonDetails;
    }
}
