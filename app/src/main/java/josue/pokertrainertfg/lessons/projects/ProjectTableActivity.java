package josue.pokertrainertfg.lessons.projects;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import josue.pokertrainer.checkers.projectsChecker.ProjectsProvider;
import josue.pokertrainer.control.sceneCreators.SceneCreator;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.Scene;
import josue.pokertrainertfg.LessonsActivity;
import josue.pokertrainertfg.Quiz;
import josue.pokertrainertfg.R;
import josue.pokertrainertfg.fillers.LessonTableFiller;
import josue.pokertrainertfg.lessons.categories.CategoryIntroductionActivity;
import josue.pokertrainertfg.lessons.kindoftables.KoTIntroductionActivity;

/**
 * Created by Josue on 08/10/14.
 */
public class ProjectTableActivity extends Activity {

    private final static String LESSONS_DATA = "LessonsData";
    private final static String LESSON4 = "Lesson4";
    private final static String RATING_LESSON3 = "RatingLesson3";
    private SharedPreferences sharedPreferences;
    private String rightAnswer;
    private Scene scene;
    private int rating = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.lesson_table);

        try {
            fillView();

            sharedPreferences = getSharedPreferences(LESSONS_DATA, MODE_PRIVATE);

            final Button[] buttons = new Button[4];
            buttons[0] = (Button) findViewById(R.id.option1);
            buttons[1] = (Button) findViewById(R.id.option2);
            buttons[2] = (Button) findViewById(R.id.option3);
            buttons[3] = (Button) findViewById(R.id.option4);

            for(int i=0; i < buttons.length; i++){
                buttons[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Button button = (Button) view;
                        if (button.getText() == rightAnswer) rating++;
                        next();
                    }
                });
            }
        } catch (Card.Exception e) {
            e.printStackTrace();
        }
    }

    private void next() {
        if(isFlop())        setTurn();
        else if (isTurn())  setRiver();
        else if(isRiver())  finishQuiz();
    }

    @Override
    protected void onResume(){
        super.onResume();
        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        // Remember that you should never show the action bar if the
        // status bar is hidden, so hide that too if necessary.
        ActionBar actionBar = getActionBar();
        actionBar.hide();
    }

    private void setTurn() {
        Card turn = scene.getTable().getDeck().takeRandomCard();
        scene.getTable().getBoard().setCard(turn);
        ImageView turnView = (ImageView) findViewById(R.id.turn);
        turnView.setImageResource(getDrawableResource(turn.getName()));
        fillRadioGroup();
    }

    private void setRiver(){
        Card river = scene.getTable().getDeck().takeRandomCard();
        scene.getTable().getBoard().setCard(river);
        ImageView turnView = (ImageView) findViewById(R.id.river);
        turnView.setImageResource(getDrawableResource(river.getName()));
        fillRadioGroup();
    }

    private void finishQuiz() {
        if(rating > sharedPreferences.getInt(RATING_LESSON3, 0))
            sharedPreferences.edit().putInt(RATING_LESSON3, rating).apply();

        ImageButton homeButton = (ImageButton) findViewById(R.id.homeButton);
        ImageButton refreshButton = (ImageButton) findViewById(R.id.refreshButton);
        ImageButton nextButton = (ImageButton) findViewById(R.id.nextPhaseButton);

        if(rating > 1){
            TextView textView = (TextView) findViewById(R.id.finishQuizTitle);
            textView.setText(R.string.levelFinished);
            sharedPreferences.edit().putBoolean(LESSON4, true).apply();
            nextButton.setEnabled(true);
            nextButton.setVisibility(View.VISIBLE);
        }
        else {
            TextView textView = (TextView) findViewById(R.id.finishQuizTitle);
            textView.setText(R.string.levelNotFinished);
            nextButton.setEnabled(false);
            nextButton.setVisibility(View.INVISIBLE);
        }
        RatingBar ratingBar = (RatingBar) findViewById(R.id.finishQuizRatingBar);
        customRatingBar(ratingBar);

        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LessonsActivity.class).addFlags(
                        Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ProjectTableActivity.class);
                startActivity(intent);
                finish();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), KoTIntroductionActivity.class).addFlags(
                        Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frameTable);
        frameLayout.bringChildToFront(findViewById(R.id.quizFinishedLayout));
        frameLayout.findViewById(R.id.lessonTable).setAlpha((float)0.5);
    }

    private void customRatingBar(RatingBar ratingBar) {
        ratingBar.setIsIndicator(true);
        ratingBar.setClickable(false);
        ratingBar.setRating(rating);
    }

    private boolean isFlop() {
        return scene.getTable().getBoard().isFlop();
    }

    private boolean isTurn() {
        return scene.getTable().getBoard().isTurn();
    }

    private boolean isRiver() {
        return scene.getTable().getBoard().isRiver();
    }

    private void fillView() throws Card.Exception {
        scene = new SceneCreator().createScene();
        LessonTableFiller lessonTableFiller = new LessonTableFiller(R.string.projectQuizQuestion, scene);
        lessonTableFiller.fillView(findViewById(R.id.lessonTable));

        fillRadioGroup();
    }

    private void fillRadioGroup() {
        String[] possibleAnswers = new String[]{getString(R.string.flushProject), getString(R.string.straightProject),
                getString(R.string.bothProjects), getString(R.string.nonProjects)};

        rightAnswer = getString(getStringResource(new ProjectsProvider().getProject(scene.getPlayer().getTexasHand(), scene.getTable().getBoard()).getProjectName()));

        ArrayList<String> quizAnswerList = new Quiz(possibleAnswers, rightAnswer).getRandomQuiz(4);

        Button button1 = (Button) findViewById(R.id.option1);
        int index = Double.valueOf(Math.random() * 100).intValue() % quizAnswerList.size();
        button1.setText(quizAnswerList.get(index));
        quizAnswerList.remove(index);

        Button button2 = (Button) findViewById(R.id.option2);
        index = Double.valueOf(Math.random() * 100).intValue() % quizAnswerList.size();
        button2.setText(quizAnswerList.get(index));
        quizAnswerList.remove(index);

        Button button3 = (Button) findViewById(R.id.option3);
        index = Double.valueOf(Math.random() * 100).intValue() % quizAnswerList.size();
        button3.setText(quizAnswerList.get(index));
        quizAnswerList.remove(index);

        Button button4 = (Button) findViewById(R.id.option4);
        index = Double.valueOf(Math.random() * 100).intValue() % quizAnswerList.size();
        button4.setText(quizAnswerList.get(index));
        quizAnswerList.remove(index);
    }

    public int getDrawableResource(String name){
        return getResources().getIdentifier(name, "drawable", getPackageName());
    }

    public int getStringResource(String name){
        return getResources().getIdentifier(name, "string", getPackageName());
    }
}
