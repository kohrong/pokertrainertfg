package josue.pokertrainertfg.lessons;

import android.app.ActionBar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;

import josue.pokertrainertfg.DetailSlidePage;
import josue.pokertrainertfg.LessonDetail;
import josue.pokertrainertfg.R;
import josue.pokertrainertfg.ScreenSlidePagerAdapter;

/**
 * Created by Josue on 06/10/14.
 */
public class DetailActivity extends FragmentActivity {

    private SharedPreferences sharedPreferences;
    private String DATA;
    private static final String UNLOCK_GO_BUTTON = ("unlockGoButton");
    private ViewPager mPager;
    private ScreenSlidePagerAdapter mPagerAdapter;
    private LessonDetail[] lessonDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lesson_detail);

        getExtras();
        sharedPreferences = getSharedPreferences(DATA, MODE_PRIVATE);
        initializeViewPager();

        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            int changed;
            @Override
            public void onPageScrolled(int i, float v, int i2) {
                if (isLastPageScrollingToRight(i)){
                    sharedPreferences.edit().putBoolean(UNLOCK_GO_BUTTON, true).apply();
                    finish();
                }
            }

            private boolean isLastPageScrollingToRight(int i) {
                return (i == lessonDetails.length-1 && changed == 1);
            }

            @Override
            public void onPageSelected(int i) {
            }

            @Override
            public void onPageScrollStateChanged(int i) {
                changed = i;
            }
        });

        Button skipButton = (Button) findViewById(R.id.skipButton);
        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharedPreferences.edit().putBoolean(UNLOCK_GO_BUTTON, true).apply();
                finish();
            }
        });
    }

    private void getExtras() {
        Intent intent = getIntent();
        Parcelable[] allParcelables = getIntent().getExtras().getParcelableArray("detail");
        lessonDetails = new LessonDetail[allParcelables.length];
        for (int i = 0; i < allParcelables.length; i++)
            lessonDetails[i] = (LessonDetail) allParcelables[i];
        DATA = intent.getStringExtra("data");
    }

    private void initializeViewPager() {
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        for (int i = 0; i < lessonDetails.length; i++)
            mPagerAdapter.addFragment(DetailSlidePage.newInstance(lessonDetails[i], 0));
        this.mPager.setAdapter(mPagerAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onResume(){
        super.onResume();
        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        // Remember that you should never show the action bar if the
        // status bar is hidden, so hide that too if necessary.
        ActionBar actionBar = getActionBar();
        actionBar.hide();
    }
}
