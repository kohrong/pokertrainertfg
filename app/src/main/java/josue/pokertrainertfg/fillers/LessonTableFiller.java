package josue.pokertrainertfg.fillers;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import josue.pokertrainer.model.Scene;
import josue.pokertrainertfg.R;

/**
 * Created by Josue on 08/10/14.
 */
public class LessonTableFiller {

    private Integer quizQuestionResource;
    private Scene scene;

    public LessonTableFiller(Integer quizQuestionResource, Scene scene) {
        this.quizQuestionResource = quizQuestionResource;
        this.scene = scene;
    }

    public void fillView(View view){
        ImageView[] imageViews = new ImageView[]{(ImageView) view.findViewById(R.id.flop0), (ImageView) view.findViewById(R.id.flop1), (ImageView) view.findViewById(R.id.flop2),
                (ImageView) view.findViewById(R.id.turn), (ImageView) view.findViewById(R.id.river)};

        for (int i = 0; i < 3; i++){
            imageViews[i].setImageResource(getDrawableResource(view, scene.getTable().getBoard().get(i).getName()));
        }

        imageViews[3].setImageResource(R.drawable.back);
        imageViews[4].setImageResource(R.drawable.back);

        ImageView[] holeCardViews = new ImageView[]{(ImageView) view.findViewById(R.id.holeCard1), (ImageView) view.findViewById(R.id.holeCard2)};
        holeCardViews[0].setImageResource(getDrawableResource(view, scene.getPlayer().getTexasHand().getCard0().getName()));
        holeCardViews[1].setImageResource(getDrawableResource(view, scene.getPlayer().getTexasHand().getCard1().getName()));

        TextView quizQuestion = (TextView) view.findViewById(R.id.quizQuestion);
        quizQuestion.setText(view.getResources().getString(quizQuestionResource));
    }

    private int getDrawableResource(View view, String name){
        return view.getResources().getIdentifier(name, "drawable", view.getContext().getPackageName());
    }

}
