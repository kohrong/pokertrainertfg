package josue.pokertrainertfg.fillers;

import android.view.View;
import android.widget.TextView;

import josue.pokertrainertfg.R;
import josue.pokertrainertfg.utils.TextViewEx;

/**
 * Created by Josue on 08/10/14.
 */
public class LessonIntroductionFiller {

    private Integer lessonTitleResource;
    private Integer lessonResumeResource;

    public LessonIntroductionFiller(Integer lessonTitleResource, Integer lessonResumeResource) {
        this.lessonTitleResource = lessonTitleResource;
        this.lessonResumeResource = lessonResumeResource;
    }

    public void fillView(View view){
        TextView textView = (TextView) view.findViewById(R.id.lessonTitle);
        textView.setText(view.getResources().getText(lessonTitleResource));

        TextViewEx textViewEx = (TextViewEx) view.findViewById(R.id.lessonResume);
        textViewEx.setText(view.getResources().getText(lessonResumeResource));
    }
}
