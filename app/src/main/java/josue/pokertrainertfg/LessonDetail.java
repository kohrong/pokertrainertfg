package josue.pokertrainertfg;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Josue on 02/10/14.
 */
public class LessonDetail implements Parcelable{

    private int[] imageResources = new int[5];
    private Integer titleResource;
    private Integer stringResource;

    public LessonDetail(int[] imageResources, Integer titleResource, Integer stringResource) {
        this.imageResources = imageResources;
        this.titleResource = titleResource;
        this.stringResource = stringResource;
    }

    public int[] getImageResources() {
        return imageResources;
    }

    public Integer getTitleResource() {
        return titleResource;
    }

    public Integer getStringResource() {
        return stringResource;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeIntArray(imageResources);
        parcel.writeInt(titleResource);
        parcel.writeInt(stringResource);
    }

    private LessonDetail(Parcel in){
        imageResources = in.createIntArray();
        titleResource = in.readInt();
        stringResource = in.readInt();
    }

    public static final Parcelable.Creator<LessonDetail> CREATOR
            = new Parcelable.Creator<LessonDetail>() {
        public LessonDetail createFromParcel(Parcel in) {
            return new LessonDetail(in);
        }

        public LessonDetail[] newArray(int size) {
            return new LessonDetail[size];
        }
    };
}
