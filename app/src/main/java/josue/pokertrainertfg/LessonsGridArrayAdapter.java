package josue.pokertrainertfg;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;

/**
 * Created by Josue on 01/10/14.
 */
public class LessonsGridArrayAdapter extends ArrayAdapter<LessonGridElement> {

    private LayoutInflater mInflater;
    private LessonGridElement[] lessonGridElements;

    public LessonsGridArrayAdapter(Context context, int resource, LessonGridElement[] lessonGridElements) {
        super(context, resource, lessonGridElements);
        this.mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.lessonGridElements = lessonGridElements;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView = mInflater.inflate(R.layout.grid_lesson, parent, false);

        ImageView lockImageView = (ImageView) rowView.findViewById(R.id.lockImage);
        lockImageView.setImageResource(lessonGridElements[position].getImageResource());

        RatingBar ratingBar = (RatingBar) rowView.findViewById(R.id.LessonsRatingBar);
        ratingBar.setRating(lessonGridElements[position].getRate());
        ratingBar.setIsIndicator(true);
        ratingBar.setClickable(false);

        rowView.setBackgroundResource(lessonGridElements[position].getBackground());

        return rowView;
    }

}
