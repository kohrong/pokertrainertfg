package josue.pokertrainertfg;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;


public class LessonsActivity extends Activity {
    private final static String LESSONS_DATA = "LessonsData";
    private final static String LESSON1 = "Lesson1";
    private final static String LESSON2 = "Lesson2";
    private final static String LESSON3 = "Lesson3";
    private final static String LESSON4 = "Lesson4";
    private final static String LESSON5 = "Lesson5";
    private final static String RATING_LESSON1 = "RatingLesson1";
    private final static String RATING_LESSON2 = "RatingLesson2";
    private final static String RATING_LESSON3 = "RatingLesson3";
    private final static String RATING_LESSON4 = "RatingLesson4";
    private final static String RATING_LESSON5 = "RatingLesson5";

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lessons);

        sharedPreferences = getSharedPreferences(LESSONS_DATA, MODE_PRIVATE);
        sharedPreferences.edit().putBoolean(LESSON1, true).apply();

        LessonGridElement[] lessonGridElements = createLessonGridElements();

        LessonsGridArrayAdapter lessonsGridArrayAdapter = new LessonsGridArrayAdapter(getApplicationContext(), R.layout.grid_lesson, lessonGridElements);

        GridView gridView = (GridView) findViewById(R.id.gridLessonView);
        gridView.setAdapter(lessonsGridArrayAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Boolean unblocked=false;
                switch (i){
                    case 0:
                        unblocked = sharedPreferences.getBoolean(LESSON1,false);
                        break;
                    case 1:
                        unblocked = sharedPreferences.getBoolean(LESSON2,false);
                        break;
                    case 2:
                        unblocked = sharedPreferences.getBoolean(LESSON3,false);
                        break;
                    case 3:
                        unblocked = sharedPreferences.getBoolean(LESSON4, false);
                        break;
                    case 4:
                        unblocked = sharedPreferences.getBoolean(LESSON5, false);
                        break;
                }

                if(unblocked){
                    LessonGridElement item = (LessonGridElement) adapterView.getItemAtPosition(i);
                    Class itemClass = null;
                    try {
                        itemClass = Class.forName(item.getActivityName());
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    Intent intent = new Intent(getApplicationContext(), itemClass);
                    startActivity(intent);
                }
            }
        });
    }

    private LessonGridElement[] createLessonGridElements() {
        LessonGridElement[] lessonGridElements = new LessonGridElement[5];

        int rate = sharedPreferences.getInt(RATING_LESSON1, 0);
        if(sharedPreferences.getBoolean(LESSON1, false)){
            lessonGridElements[0] = new LessonGridElement("josue.pokertrainertfg.lessons.streets.StreetIntroductionActivity", rate, R.drawable.ic_uno, true);
        }
        else lessonGridElements[0] = new LessonGridElement("josue.pokertrainertfg.lessons.streets.StreetIntroductionActivity", rate, R.drawable.ic_locked,  false);

        rate = sharedPreferences.getInt(RATING_LESSON2, 0);
        if(sharedPreferences.getBoolean(LESSON2, false)){
            lessonGridElements[1] = new LessonGridElement("josue.pokertrainertfg.lessons.categories.CategoryIntroductionActivity", rate, R.drawable.ic_dos, true);
        }
        else lessonGridElements[1] = new LessonGridElement("josue.pokertrainertfg.lessons.categories.CategoryIntroductionActivity", rate, R.drawable.ic_locked, false);

        rate = sharedPreferences.getInt(RATING_LESSON3, 0);
        if(sharedPreferences.getBoolean(LESSON3, false)){
            lessonGridElements[2] = new LessonGridElement("josue.pokertrainertfg.lessons.projects.ProjectIntroductionActivity", rate, R.drawable.ic_tres, true);
        }
        else lessonGridElements[2] = new LessonGridElement("josue.pokertrainertfg.lessons.projects.ProjectIntroductionActivity", rate,R.drawable.ic_locked, false);

        rate = sharedPreferences.getInt(RATING_LESSON4, 0);
        if(sharedPreferences.getBoolean(LESSON4, false)){
            lessonGridElements[3] = new LessonGridElement("josue.pokertrainertfg.lessons.kindoftables.KoTIntroductionActivity", rate, R.drawable.ic_cuatro, true);
        }
        else lessonGridElements[3] = new LessonGridElement("josue.pokertrainertfg.lessons.kindoftables.KoTIntroductionActivity", rate, R.drawable.ic_locked, false);

        rate = sharedPreferences.getInt(RATING_LESSON5, 0);
        if(sharedPreferences.getBoolean(LESSON5, false)){
            lessonGridElements[4] = new LessonGridElement("josue.pokertrainertfg.lessons.handstrength.HandStrengthIntroductionActivity", rate, R.drawable.ic_cinco, true);
        }
        else lessonGridElements[4] = new LessonGridElement("josue.pokertrainertfg.lessons.handstrength.HandStrengthIntroductionActivity", rate,R.drawable.ic_locked, false);

        return lessonGridElements;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.lessons, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume(){
        super.onResume();
        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        // Remember that you should never show the action bar if the
        // status bar is hidden, so hide that too if necessary.
        ActionBar actionBar = getActionBar();
        actionBar.hide();
    }
}
