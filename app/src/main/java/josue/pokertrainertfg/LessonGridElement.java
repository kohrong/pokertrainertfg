package josue.pokertrainertfg;

/**
 * Created by Josue on 01/10/14.
 */
public class LessonGridElement {
    private String activityName;
    private Integer imageResource;
    private Integer rate;
    private Integer background;

    public LessonGridElement(String activityName, int rate, int imageResource, boolean unblocked) {
        if (unblocked){
            this.background = R.drawable.main_lessons_border;
            this.imageResource = imageResource;
        }
        else {
            this.background = R.drawable.main_lessons_border_disabled;
            this.imageResource = R.drawable.ic_locked;
        }
        this.activityName = activityName;
        this.rate = rate;

    }

    public String getActivityName() {
        return activityName;
    }

    public Integer getImageResource() {
        return imageResource;
    }

    public Integer getRate() {
        return rate;
    }

    public Integer getBackground() {
        return background;
    }
}
