package josue.pokertrainertfg.test.cardListAttributesChecker;

import android.test.InstrumentationTestCase;

import java.util.ArrayList;

import josue.pokertrainer.cardListAttributes.SameSuit;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;

/**
 * Created by Josue on 19/08/14.
 */
public class SameSuitCheckerTest extends InstrumentationTestCase {

    public void testIsRainbow() throws Card.Exception {
        ArrayList<Card> sequence = new ArrayList<>();
        sequence.add(new Card(3, CardSuit.CLUB));
        sequence.add(new Card(4, CardSuit.DIAMOND));
        sequence.add(new Card(5, CardSuit.HEART));

        assertTrue(new SameSuit().check(sequence, 0));
    }

    public void testIsFlushProject() throws Card.Exception {
        ArrayList<Card> sequence = new ArrayList<>();
        sequence.add(new Card(3, CardSuit.CLUB));
        sequence.add(new Card(4, CardSuit.CLUB));
        sequence.add(new Card(5, CardSuit.HEART));

        assertTrue(new SameSuit().check(sequence, 2));
    }

    public void testFlushIsPossible() throws Card.Exception {
        ArrayList<Card> sequence = new ArrayList<>();
        sequence.add(new Card(3, CardSuit.CLUB));
        sequence.add(new Card(4, CardSuit.CLUB));
        sequence.add(new Card(5, CardSuit.CLUB));

        assertTrue(new SameSuit().check(sequence, 3));
    }

    public void testTwoFlushProjects() throws Card.Exception {
        ArrayList<Card> sequence = new ArrayList<>();
        sequence.add(new Card(3, CardSuit.CLUB));
        sequence.add(new Card(4, CardSuit.CLUB));
        sequence.add(new Card(5, CardSuit.HEART));
        sequence.add(new Card(6, CardSuit.HEART));

        assertTrue(new SameSuit().check(sequence, 2));
    }

    public void testOneProjectAndOnePossibleFlush() throws Card.Exception {
        ArrayList<Card> sequence = new ArrayList<>();
        sequence.add(new Card(3, CardSuit.CLUB));
        sequence.add(new Card(4, CardSuit.CLUB));
        sequence.add(new Card(5, CardSuit.HEART));
        sequence.add(new Card(6, CardSuit.HEART));
        sequence.add(new Card(7, CardSuit.HEART));

        assertTrue(new SameSuit().check(sequence, 3));
    }
}
