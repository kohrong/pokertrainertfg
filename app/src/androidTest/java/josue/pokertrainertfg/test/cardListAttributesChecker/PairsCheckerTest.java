package josue.pokertrainertfg.test.cardListAttributesChecker;

import android.test.InstrumentationTestCase;



import java.util.ArrayList;

import josue.pokertrainer.cardListAttributes.Pairs;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;

/**
 * Created by Josue on 19/08/14.
 */
public class PairsCheckerTest extends InstrumentationTestCase {

    public void testUnpaired() throws Card.Exception {
        ArrayList<Card> sequence = new ArrayList<>();
        sequence.add(new Card(1, CardSuit.HEART));
        sequence.add(new Card(2, CardSuit.HEART));
        sequence.add(new Card(5, CardSuit.HEART));

        assertTrue(new Pairs().check(sequence, 0));
    }

    public void testOnePair() throws Card.Exception {
        ArrayList<Card> sequence = new ArrayList<>();
        sequence.add(new Card(1, CardSuit.HEART));
        sequence.add(new Card(2, CardSuit.HEART));
        sequence.add(new Card(5, CardSuit.HEART));
        sequence.add(new Card(5, CardSuit.CLUB));

        assertTrue(new Pairs().check(sequence, 1));
    }

    public void testTwoPairs() throws Card.Exception {
        ArrayList<Card> sequence = new ArrayList<>();
        sequence.add(new Card(1, CardSuit.HEART));
        sequence.add(new Card(1, CardSuit.SPADE));
        sequence.add(new Card(5, CardSuit.HEART));
        sequence.add(new Card(5, CardSuit.CLUB));

        assertTrue(new Pairs().check(sequence, 2));
    }
}
