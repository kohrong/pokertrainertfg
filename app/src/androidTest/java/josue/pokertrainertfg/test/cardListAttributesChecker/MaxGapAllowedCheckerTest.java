package josue.pokertrainertfg.test.cardListAttributesChecker;

import android.test.InstrumentationTestCase;



import java.util.ArrayList;

import josue.pokertrainer.cardListAttributes.MaxGapAllowed;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;

/**
 * Created by Josue on 19/08/14.
 */
public class MaxGapAllowedCheckerTest extends InstrumentationTestCase {

    public void testGapIsZero() throws Card.Exception {
        ArrayList<Card> sequence = new ArrayList<>();
        sequence.add(new Card(3, CardSuit.CLUB));
        sequence.add(new Card(3, CardSuit.SPADE));
        sequence.add(new Card(3, CardSuit.HEART));
        sequence.add(new Card(3, CardSuit.DIAMOND));

        assertFalse(new MaxGapAllowed().check(sequence, 0));
    }

    public void testGapIsOne() throws Card.Exception {
        ArrayList<Card> sequence = new ArrayList<>();
        sequence.add(new Card(3, CardSuit.CLUB));
        sequence.add(new Card(4, CardSuit.CLUB));
        sequence.add(new Card(5, CardSuit.HEART));
        sequence.add(new Card(6, CardSuit.HEART));

        assertTrue(new MaxGapAllowed().check(sequence, 1));
    }

    public void testGapIsOneWithLowerAce() throws Card.Exception {
        ArrayList<Card> sequence = new ArrayList<>();
        sequence.add(new Card('a', CardSuit.CLUB));
        sequence.add(new Card(2, CardSuit.CLUB));
        sequence.add(new Card(5, CardSuit.HEART));
        sequence.add(new Card(9, CardSuit.HEART));

        assertTrue(new MaxGapAllowed().check(sequence, 1));
    }

    public void testGapIsOneWithHigherAce() throws Card.Exception {
        ArrayList<Card> sequence = new ArrayList<>();
        sequence.add(new Card('a', CardSuit.CLUB));
        sequence.add(new Card('k', CardSuit.CLUB));
        sequence.add(new Card(5, CardSuit.HEART));
        sequence.add(new Card(9, CardSuit.HEART));

        assertTrue(new MaxGapAllowed().check(sequence, 1));
    }
}
