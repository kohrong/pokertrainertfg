package josue.pokertrainertfg.test.cardListAttributesChecker;

import android.test.InstrumentationTestCase;



import java.util.ArrayList;

import josue.pokertrainer.cardListAttributes.ConsecutiveCards;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;

/**
 * Created by Josue on 20/08/14.
 */
public class ConsecutiveCardCheckerTest extends InstrumentationTestCase {

    public void testNonConsecutiveCards() throws Card.Exception {
        ArrayList<Card> sequence = new ArrayList<>();
        sequence.add(new Card(1, CardSuit.CLUB));
        sequence.add(new Card(3, CardSuit.CLUB));
        sequence.add(new Card(5, CardSuit.CLUB));
        sequence.add(new Card(7, CardSuit.CLUB));

        assertTrue(new ConsecutiveCards().check(sequence, 0));
    }

    public void testConsecutiveCards() throws Card.Exception {
        ArrayList<Card> sequence = new ArrayList<>();
        sequence.add(new Card(1, CardSuit.CLUB));
        sequence.add(new Card(2, CardSuit.CLUB));
        sequence.add(new Card(5, CardSuit.CLUB));
        sequence.add(new Card(7, CardSuit.CLUB));

        assertTrue(new ConsecutiveCards().check(sequence, 1));
    }

    public void testDoubleConsecutiveCards() throws Card.Exception {
        ArrayList<Card> sequence = new ArrayList<>();
        sequence.add(new Card(1, CardSuit.CLUB));
        sequence.add(new Card(2, CardSuit.CLUB));
        sequence.add(new Card(5, CardSuit.CLUB));
        sequence.add(new Card(6, CardSuit.CLUB));

        assertTrue(new ConsecutiveCards().check(sequence, 1));
    }

    public void testConsecutiveRoyalCards() throws Card.Exception {
        ArrayList<Card> sequence = new ArrayList<>();
        sequence.add(new Card(1, CardSuit.CLUB));
        sequence.add(new Card(13, CardSuit.CLUB));
        sequence.add(new Card(12, CardSuit.CLUB));
        sequence.add(new Card(7, CardSuit.CLUB));

        assertTrue(new ConsecutiveCards().check(sequence, 3));
    }

    public void testConsecutiveCyclicCards() throws Card.Exception {
        ArrayList<Card> sequence = new ArrayList<>();
        sequence.add(new Card(1, CardSuit.CLUB));
        sequence.add(new Card(13, CardSuit.CLUB));
        sequence.add(new Card(12, CardSuit.CLUB));
        sequence.add(new Card(2, CardSuit.CLUB));

        assertTrue(new ConsecutiveCards().check(sequence, 3));
    }
}
