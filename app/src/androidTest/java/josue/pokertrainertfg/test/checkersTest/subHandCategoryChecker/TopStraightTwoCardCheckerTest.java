package josue.pokertrainertfg.test.checkersTest.subHandCategoryChecker;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.checkers.subHandCategoryChecker.TopStraightTwoCardChecker;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.TexasHand;


/**
 * Created by Josue on 27/08/14.
 */
public class TopStraightTwoCardCheckerTest extends InstrumentationTestCase {
    public void testIsTopStraightTwoCards() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card(6, CardSuit.CLUB), new Card(9, CardSuit.DIAMOND));

        Board board = new Board(new Card[]{new Card(8, CardSuit.HEART), new Card(7, CardSuit.SPADE),
            new Card(5, CardSuit.SPADE)});

        assertTrue(new TopStraightTwoCardChecker().check(texasHand, board));
    }

    public void testIsNotTopStraightTwoCards() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('j', CardSuit.CLUB), new Card(3, CardSuit.DIAMOND));

        Board board = new Board(new Card[]{new Card(8, CardSuit.HEART), new Card(7, CardSuit.SPADE),
                new Card(9, CardSuit.SPADE)}, new Card('t', CardSuit.SPADE));

        assertFalse(new TopStraightTwoCardChecker().check(texasHand, board));
    }

    public void testIsSuperTopStraightTwoCards() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('j', CardSuit.CLUB), new Card('q', CardSuit.DIAMOND));

        Board board = new Board(new Card[]{new Card(8, CardSuit.HEART), new Card(7, CardSuit.SPADE),
                new Card(9, CardSuit.SPADE)}, new Card('t', CardSuit.SPADE));

        assertTrue(new TopStraightTwoCardChecker().check(texasHand, board));
    }

    public void testIsSpecialTopStraightTwoCards() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card(4, CardSuit.CLUB), new Card(5, CardSuit.DIAMOND));

        Board board = new Board(new Card[]{new Card(2, CardSuit.HEART), new Card(3, CardSuit.SPADE),
                new Card('a', CardSuit.SPADE)});

        assertTrue(new TopStraightTwoCardChecker().check(texasHand, board));
    }
}
