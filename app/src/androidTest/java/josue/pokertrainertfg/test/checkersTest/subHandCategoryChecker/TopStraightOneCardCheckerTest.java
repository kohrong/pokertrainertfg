package josue.pokertrainertfg.test.checkersTest.subHandCategoryChecker;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.checkers.subHandCategoryChecker.TopStraightOneCardChecker;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.TexasHand;


/**
 * Created by Josue on 28/08/14.
 */
public class TopStraightOneCardCheckerTest extends InstrumentationTestCase {
    public void testIsTopStraightOneCard() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('a', CardSuit.CLUB), new Card(9, CardSuit.DIAMOND));

        Board board = new Board(new Card[]{new Card(8, CardSuit.HEART), new Card(7, CardSuit.SPADE),
                new Card(5, CardSuit.SPADE)}, new Card(6, CardSuit.DIAMOND));

        assertTrue(new TopStraightOneCardChecker().check(texasHand, board));
    }

    public void testIsTopStraightOneCardWithJack() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('j', CardSuit.CLUB), new Card(3, CardSuit.DIAMOND));

        Board board = new Board(new Card[]{new Card(8, CardSuit.HEART), new Card(7, CardSuit.SPADE),
                new Card(9, CardSuit.SPADE)}, new Card('t', CardSuit.SPADE));

        assertTrue(new TopStraightOneCardChecker().check(texasHand, board));
    }

    public void testIsSuperTopStraightOneCard() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('j', CardSuit.CLUB), new Card('q', CardSuit.DIAMOND));

        Board board = new Board(new Card[]{new Card(8, CardSuit.HEART), new Card(7, CardSuit.SPADE),
                new Card(9, CardSuit.SPADE)}, new Card('t', CardSuit.SPADE));

        assertFalse(new TopStraightOneCardChecker().check(texasHand, board));
    }

    public void testIsSpecialTopStraightOneCard() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card(9, CardSuit.CLUB), new Card(5, CardSuit.DIAMOND));

        Board board = new Board(new Card[]{new Card(2, CardSuit.HEART), new Card(3, CardSuit.SPADE),
                new Card('a', CardSuit.SPADE)}, new Card(4, CardSuit.CLUB));

        assertTrue(new TopStraightOneCardChecker().check(texasHand, board));
    }
}
