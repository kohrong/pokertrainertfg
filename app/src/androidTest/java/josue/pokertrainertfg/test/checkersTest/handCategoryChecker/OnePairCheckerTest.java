package josue.pokertrainertfg.test.checkersTest.handCategoryChecker;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.checkers.handCategoryChecker.OnePairChecker;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.PokerHand;

public class OnePairCheckerTest extends InstrumentationTestCase {
    
    public OnePairCheckerTest() {
    }


    public void testCheck() throws Card.Exception {
        OnePairChecker onePairChecker = new OnePairChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card(1, CardSuit.CLUB),
            new Card(2, CardSuit.CLUB), new Card(3, CardSuit.CLUB), new Card(4, CardSuit.CLUB), 
            new Card(7, CardSuit.DIAMOND)});
        assertFalse(onePairChecker.check(pokerHand));
    }
    

    public void testIsPair() throws Card.Exception{
        OnePairChecker onePairChecker = new OnePairChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card(1, CardSuit.CLUB), 
            new Card(1, CardSuit.DIAMOND), new Card(3, CardSuit.CLUB), new Card(4, CardSuit.CLUB), 
            new Card(7, CardSuit.DIAMOND)});
        assertTrue(onePairChecker.check(pokerHand));
    }
    

    public void testIsThreeOfAKind() throws Card.Exception{
        OnePairChecker onePairChecker = new OnePairChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card(1, CardSuit.CLUB), 
            new Card(1, CardSuit.DIAMOND), new Card(1, CardSuit.HEART), new Card(4, CardSuit.CLUB), 
            new Card(7, CardSuit.DIAMOND)});
        assertTrue(onePairChecker.check(pokerHand));
    }
}
