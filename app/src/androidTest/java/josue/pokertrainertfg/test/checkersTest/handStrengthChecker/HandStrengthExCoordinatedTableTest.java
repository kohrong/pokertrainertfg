package josue.pokertrainertfg.test.checkersTest.handStrengthChecker;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.checkers.handStrengthChecker.HandStrength;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.TexasHand;


/**
 * Created by Josue on 28/08/14.
 */
public class HandStrengthExCoordinatedTableTest extends InstrumentationTestCase {

    public void testNothingHandWithPocketPair() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('k', CardSuit.DIAMOND), new Card('k', CardSuit.HEART));

        Board board = new Board(new Card[]{new Card('j', CardSuit.SPADE), new Card(9, CardSuit.CLUB),
            new Card(8, CardSuit.CLUB)}, new Card(7, CardSuit.DIAMOND));

        assertTrue(new HandStrength().getStrength(texasHand, board) == HandStrength.Strength.nothing);
    }

    public void testVeryStrongHandGoesToStrong() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('a', CardSuit.CLUB), new Card('q', CardSuit.HEART));

        Board board = new Board(new Card[]{new Card('k', CardSuit.CLUB), new Card(9, CardSuit.CLUB),
                new Card(4, CardSuit.CLUB)}, new Card(2, CardSuit.CLUB));

        assertTrue(new HandStrength().getStrength(texasHand, board) == HandStrength.Strength.veryStrong);

        board.setCard(new Card(2, CardSuit.DIAMOND));

        assertTrue(new HandStrength().getStrength(texasHand, board) == HandStrength.Strength.strong);
    }

    public void testIsWeakHand() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('j', CardSuit.DIAMOND), new Card('j', CardSuit.CLUB));

        Board board = new Board(new Card[]{new Card('j', CardSuit.HEART), new Card('k', CardSuit.HEART),
                new Card(8, CardSuit.HEART)}, new Card(3, CardSuit.HEART));

        assertTrue(new HandStrength().getStrength(texasHand, board) == HandStrength.Strength.weak);
    }

    public void testIsNothingHand() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card(8, CardSuit.DIAMOND), new Card(8, CardSuit.CLUB));

        Board board = new Board(new Card[]{new Card('j', CardSuit.HEART), new Card(7, CardSuit.HEART),
                new Card(5, CardSuit.HEART)}, new Card(3, CardSuit.HEART));

        assertTrue(new HandStrength().getStrength(texasHand, board) == HandStrength.Strength.nothing);
    }
}
