package josue.pokertrainertfg.test.checkersTest.subHandCategoryChecker;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.checkers.subHandCategoryChecker.BestTwoPairChecker;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.TexasHand;


/**
 * Created by Josue on 26/08/14.
 */
public class BestTwoPairCheckerTest extends InstrumentationTestCase {

    public void testIsNotBestTwoPair() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('a', CardSuit.HEART), new Card('a', CardSuit.SPADE));

        Board board = new Board(new Card[]{new Card('k', CardSuit.CLUB), new Card(8, CardSuit.CLUB),
                new Card(2, CardSuit.DIAMOND)}, new Card('k', CardSuit.DIAMOND));

        assertFalse(new BestTwoPairChecker().check(texasHand, board));
    }

    public void testIsBestTwoPair() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('a', CardSuit.HEART), new Card('k', CardSuit.SPADE));

        Board board = new Board(new Card[]{new Card('k', CardSuit.CLUB), new Card(8, CardSuit.CLUB),
                new Card(8, CardSuit.DIAMOND)}, new Card('a', CardSuit.DIAMOND));

        assertTrue(new BestTwoPairChecker().check(texasHand, board));
    }
}
