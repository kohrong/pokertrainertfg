package josue.pokertrainertfg.test.checkersTest.handCategoryChecker;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.checkers.handCategoryChecker.StraightChecker;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.PokerHand;

public class StraightCheckerTest extends InstrumentationTestCase {
    
    public StraightCheckerTest() {
    }


    public void testCheck() throws Card.Exception {
        StraightChecker straightChecker = new StraightChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card(1, CardSuit.CLUB),
            new Card(2, CardSuit.CLUB), new Card(3, CardSuit.CLUB), new Card(4, CardSuit.CLUB), 
            new Card(7, CardSuit.DIAMOND)});
        assertFalse(straightChecker.check(pokerHand));
    }
    

    public void testIsStraightWithAce() throws Card.Exception{
        StraightChecker straightChecker = new StraightChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card(1, CardSuit.CLUB), 
            new Card(2, CardSuit.CLUB), new Card(3, CardSuit.CLUB), new Card(4, CardSuit.CLUB), 
            new Card(5, CardSuit.DIAMOND)});
        assertTrue(straightChecker.check(pokerHand));
    }
      

    public void testIsRoyalStraight() throws Card.Exception{
        StraightChecker straightChecker = new StraightChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card('t', CardSuit.CLUB),
            new Card('k', CardSuit.CLUB), new Card('q', CardSuit.CLUB), new Card('j', CardSuit.CLUB),
            new Card('a', CardSuit.DIAMOND)});
        assertTrue(straightChecker.check(pokerHand));
    }
    

    public void testMinimumStraight() throws Card.Exception{
        StraightChecker straightChecker = new StraightChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card(1, CardSuit.CLUB), 
            new Card(2, CardSuit.CLUB), new Card(3, CardSuit.CLUB), new Card(4, CardSuit.CLUB), 
            new Card(5, CardSuit.CLUB)});
        assertTrue(straightChecker.check(pokerHand));
    }
}
