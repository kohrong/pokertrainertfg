package josue.pokertrainertfg.test.checkersTest.handCategoryChecker;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.checkers.handCategoryChecker.TwoPairChecker;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.PokerHand;


public class TwoPairCheckerTest extends InstrumentationTestCase {
    
    public TwoPairCheckerTest() {
    }


    public void testCheck() throws Card.Exception {
        TwoPairChecker twoPairChecker = new TwoPairChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card(1, CardSuit.CLUB),
            new Card(2, CardSuit.CLUB), new Card(3, CardSuit.CLUB), new Card(4, CardSuit.CLUB), 
            new Card(7, CardSuit.DIAMOND)});
        assertFalse(twoPairChecker.check(pokerHand));
    }
    

    public void testIsTwoPair() throws Card.Exception{
        TwoPairChecker twoPairChecker = new TwoPairChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card(1, CardSuit.CLUB), 
            new Card(1, CardSuit.DIAMOND), new Card(3, CardSuit.CLUB), new Card(3, CardSuit.DIAMOND), 
            new Card(7, CardSuit.DIAMOND)});
        assertTrue(twoPairChecker.check(pokerHand));
    }
    

    public void testIsFullHouse() throws Card.Exception{
        TwoPairChecker twoPairChecker = new TwoPairChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card(1, CardSuit.CLUB), 
            new Card(1, CardSuit.DIAMOND), new Card(3, CardSuit.CLUB), new Card(3, CardSuit.DIAMOND), 
            new Card(1, CardSuit.HEART)});
        assertTrue(twoPairChecker.check(pokerHand));
    }
    

    public void testIsThreeOfAKind() throws Card.Exception{
        TwoPairChecker twoPairChecker = new TwoPairChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card(1, CardSuit.CLUB), 
            new Card(1, CardSuit.DIAMOND), new Card(1, CardSuit.HEART), new Card(3, CardSuit.DIAMOND), 
            new Card(7, CardSuit.DIAMOND)});
        assertFalse(twoPairChecker.check(pokerHand));
    }
    
}
