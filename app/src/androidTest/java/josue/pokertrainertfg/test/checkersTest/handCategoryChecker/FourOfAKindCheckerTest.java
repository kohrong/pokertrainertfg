package josue.pokertrainertfg.test.checkersTest.handCategoryChecker;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.checkers.handCategoryChecker.FourOfAKindChecker;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.PokerHand;


public class FourOfAKindCheckerTest extends InstrumentationTestCase{
    
    public FourOfAKindCheckerTest() {
    }

    public void testCheck() throws Card.Exception {
        FourOfAKindChecker fourOfAKindChecker = new FourOfAKindChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card(1, CardSuit.CLUB),
            new Card(2, CardSuit.CLUB), new Card(3, CardSuit.CLUB), new Card(4, CardSuit.CLUB),
            new Card(7, CardSuit.DIAMOND)});
        assertFalse(fourOfAKindChecker.check(pokerHand));
    }

    public void testIsFourOfAKind() throws Card.Exception{
        FourOfAKindChecker fourOfAKindChecker = new FourOfAKindChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card(1, CardSuit.CLUB), 
            new Card(1, CardSuit.DIAMOND), new Card(1, CardSuit.HEART), new Card(1, CardSuit.SPADE), 
            new Card(7, CardSuit.DIAMOND)});
        assertTrue(fourOfAKindChecker.check(pokerHand));
    }
    
}
