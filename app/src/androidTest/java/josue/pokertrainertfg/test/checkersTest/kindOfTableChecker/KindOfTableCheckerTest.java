package josue.pokertrainertfg.test.checkersTest.kindOfTableChecker;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.checkers.kindOfTableChecker.CoordinatedChecker;
import josue.pokertrainer.checkers.kindOfTableChecker.ExtremelyCoordinatedChecker;
import josue.pokertrainer.checkers.kindOfTableChecker.HalfCoordinatedChecker;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;


public class KindOfTableCheckerTest extends InstrumentationTestCase {


	public void testIsExtremelyCoordinatedStraight() throws Exception {
		Board board = new Board(new Card[]{new Card('j', CardSuit.CLUB), new Card(10, CardSuit.SPADE),
				new Card(8, CardSuit.CLUB)}, new Card(7, CardSuit.DIAMOND));
		
		assertTrue(new ExtremelyCoordinatedChecker().check(board));
	}


	public void testIsExtremelyCoordinatedFlush() throws Exception{
		Board board = new Board(new Card[]{new Card('a', CardSuit.CLUB), new Card(7, CardSuit.CLUB),
				new Card(8, CardSuit.CLUB)}, new Card(2, CardSuit.CLUB));
		
		assertTrue(new ExtremelyCoordinatedChecker().check(board));
	}
	

	public void testIsCoordinatedStraight() throws Exception{
		Board board = new Board(new Card[]{new Card('j', CardSuit.HEART), new Card(8, CardSuit.SPADE),
				new Card(7, CardSuit.CLUB)});
		
		assertTrue(new CoordinatedChecker().check(board));
	}
	

	public void testIsCoordinatedFlush() throws Exception{
		Board board = new Board(new Card[]{new Card(4, CardSuit.SPADE), new Card(10, CardSuit.DIAMOND),
				new Card(8, CardSuit.DIAMOND)}, new Card(2, CardSuit.DIAMOND));
		
		assertTrue(new CoordinatedChecker().check(board));
	}
	

	public void testIsNotCoordinated() throws Exception{
		Board board = new Board(new Card[]{new Card(10, CardSuit.CLUB), new Card(9, CardSuit.DIAMOND),
				new Card(2, CardSuit.HEART)});
		
		assertFalse(new CoordinatedChecker().check(board));
	}
	

	public void testIsHalfCoordinatedNotFlushProject() throws Exception{
		Board board = new Board(new Card[]{new Card('j', CardSuit.CLUB), new Card(8, CardSuit.DIAMOND),
				new Card(5, CardSuit.HEART)});
		
		assertTrue(new HalfCoordinatedChecker().check(board));
		
		board.setFlop(new Card[]{new Card(9, CardSuit.CLUB), new Card(6, CardSuit.DIAMOND),
				new Card('k', CardSuit.HEART)});
		
		assertTrue(new HalfCoordinatedChecker().check(board));
		
		board.setFlop(new Card[]{new Card('k', CardSuit.CLUB), new Card('t', CardSuit.DIAMOND),
				new Card(6, CardSuit.HEART)});
		
		assertTrue(new HalfCoordinatedChecker().check(board));
	}
	

	public void testIsHalfCoordinatedFlushProject() throws Exception{
		Board board = new Board(new Card[]{new Card('j', CardSuit.CLUB), new Card(7, CardSuit.DIAMOND),
				new Card(5, CardSuit.CLUB)});
		
		assertTrue(new HalfCoordinatedChecker().check(board));
		
		board.setFlop(new Card[]{new Card(10, CardSuit.HEART), new Card(6, CardSuit.DIAMOND),
				new Card('a', CardSuit.HEART)});
		
		assertTrue(new HalfCoordinatedChecker().check(board));
		
		board.setFlop(new Card[]{new Card('a', CardSuit.DIAMOND), new Card('t', CardSuit.DIAMOND),
				new Card(5, CardSuit.HEART)});
		
		assertTrue(new HalfCoordinatedChecker().check(board));
	}
}
