package josue.pokertrainertfg.test.checkersTest.subHandCategoryChecker;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.checkers.subHandCategoryChecker.BestFlushChecker;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.TexasHand;


/**
 * Created by Josue on 26/08/14.
 */
public class BestFlushCheckerTest extends InstrumentationTestCase {

    public void testIsBestFlushWithKing() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('k', CardSuit.DIAMOND), new Card(8, CardSuit.DIAMOND));

        Board board = new Board(new Card[]{new Card('a', CardSuit.DIAMOND), new Card(7, CardSuit.DIAMOND),
            new Card('t', CardSuit.DIAMOND)});

        assertTrue(new BestFlushChecker().check(texasHand, board));
    }

    public void testIsBestFlush() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('k', CardSuit.DIAMOND), new Card('q', CardSuit.DIAMOND));

        Board board = new Board(new Card[]{new Card('a', CardSuit.DIAMOND), new Card(7, CardSuit.DIAMOND),
                new Card('t', CardSuit.DIAMOND)});

        assertTrue(new BestFlushChecker().check(texasHand, board));
    }
}
