package josue.pokertrainertfg.test.checkersTest.subHandCategoryChecker;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.checkers.subHandCategoryChecker.SecondFlushChecker;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.TexasHand;


/**
 * Created by Josue on 27/08/14.
 */
public class SecondFlushCheckerTest extends InstrumentationTestCase {

    public void testIsSecondFlush() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('k', CardSuit.DIAMOND), new Card(10, CardSuit.CLUB));

        Board board = new Board(new Card[]{new Card(9, CardSuit.DIAMOND), new Card('q', CardSuit.DIAMOND),
        new Card('j', CardSuit.DIAMOND)}, new Card(6, CardSuit.DIAMOND));

        assertTrue(new SecondFlushChecker().check(texasHand, board));
    }

    public void testIsSecondFlushLowerBoard() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('k', CardSuit.DIAMOND), new Card('q', CardSuit.DIAMOND));

        Board board = new Board(new Card[]{new Card(2, CardSuit.DIAMOND), new Card(3, CardSuit.DIAMOND),
                new Card(5, CardSuit.DIAMOND)}, new Card(6, CardSuit.DIAMOND));

        assertTrue(new SecondFlushChecker().check(texasHand, board));
    }

    public void testIsSecondFlushWithJack() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('j', CardSuit.DIAMOND), new Card('t', CardSuit.DIAMOND));

        Board board = new Board(new Card[]{new Card('a', CardSuit.DIAMOND), new Card(9, CardSuit.DIAMOND),
                new Card('q', CardSuit.DIAMOND)}, new Card(6, CardSuit.DIAMOND));

        assertTrue(new SecondFlushChecker().check(texasHand, board));
    }

}
