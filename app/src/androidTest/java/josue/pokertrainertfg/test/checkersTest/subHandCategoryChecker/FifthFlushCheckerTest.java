package josue.pokertrainertfg.test.checkersTest.subHandCategoryChecker;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.checkers.subHandCategoryChecker.FifthFlushChecker;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.TexasHand;


/**
 * Created by Josue on 27/08/14.
 */
public class FifthFlushCheckerTest extends InstrumentationTestCase {
    public void testIsSecondFlush() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card(7, CardSuit.DIAMOND), new Card(2, CardSuit.DIAMOND));

        Board board = new Board(new Card[]{new Card(9, CardSuit.DIAMOND), new Card('q', CardSuit.DIAMOND),
                new Card('j', CardSuit.DIAMOND)}, new Card(6, CardSuit.DIAMOND));

        assertTrue(new FifthFlushChecker().check(texasHand, board));
    }

    public void testIsFifthFlushLowerBoard() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('t', CardSuit.DIAMOND), new Card(9, CardSuit.DIAMOND));

        Board board = new Board(new Card[]{new Card(2, CardSuit.DIAMOND), new Card(3, CardSuit.DIAMOND),
                new Card(5, CardSuit.DIAMOND)}, new Card(6, CardSuit.DIAMOND));

        assertTrue(new FifthFlushChecker().check(texasHand, board));
    }

    public void testIsFifthFlushWithKing() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('7', CardSuit.DIAMOND), new Card(2, CardSuit.DIAMOND));

        Board board = new Board(new Card[]{new Card('a', CardSuit.DIAMOND), new Card(9, CardSuit.DIAMOND),
                new Card('q', CardSuit.DIAMOND)}, new Card(6, CardSuit.DIAMOND));

        assertTrue(new FifthFlushChecker().check(texasHand, board));
    }

    public void testNotFifthFlush() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card(5, CardSuit.DIAMOND), new Card(2, CardSuit.DIAMOND));

        Board board = new Board(new Card[]{new Card('a', CardSuit.DIAMOND), new Card(9, CardSuit.DIAMOND),
                new Card('q', CardSuit.DIAMOND)}, new Card(8, CardSuit.DIAMOND), new Card('k', CardSuit.DIAMOND));

        assertFalse(new FifthFlushChecker().check(texasHand, board));
    }
}
