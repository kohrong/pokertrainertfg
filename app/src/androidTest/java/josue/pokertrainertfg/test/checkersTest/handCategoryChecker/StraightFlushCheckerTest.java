package josue.pokertrainertfg.test.checkersTest.handCategoryChecker;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.checkers.handCategoryChecker.StraightFlushChecker;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.PokerHand;

public class StraightFlushCheckerTest extends InstrumentationTestCase {
    
    public StraightFlushCheckerTest() {
    }


    public void testCheck() throws Card.Exception {
        StraightFlushChecker straightFlushChecker = new StraightFlushChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card(1, CardSuit.CLUB),
            new Card(2, CardSuit.CLUB), new Card(3, CardSuit.CLUB), new Card(4, CardSuit.CLUB),
            new Card(7, CardSuit.CLUB)});
        assertFalse(straightFlushChecker.check(pokerHand));
    }
    

    public void testIsStraightFlush() throws Card.Exception{
        StraightFlushChecker straightFlushChecker = new StraightFlushChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card(1, CardSuit.CLUB), 
            new Card(2, CardSuit.CLUB), new Card(3, CardSuit.CLUB), new Card(4, CardSuit.CLUB), 
            new Card(5, CardSuit.CLUB)});
        assertTrue(straightFlushChecker.check(pokerHand));
    }
    

    public void testIsFlushIsntStraight() throws Card.Exception{
        StraightFlushChecker straightFlushChecker = new StraightFlushChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card(1, CardSuit.CLUB), 
            new Card(2, CardSuit.CLUB), new Card(8, CardSuit.CLUB), new Card(4, CardSuit.CLUB), 
            new Card(5, CardSuit.CLUB)});
        assertFalse(straightFlushChecker.check(pokerHand));
    }
    

    public void testIsStraightIsntFlush() throws Card.Exception{
        StraightFlushChecker straightFlushChecker = new StraightFlushChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card(1, CardSuit.CLUB), 
            new Card(2, CardSuit.CLUB), new Card(3, CardSuit.CLUB), new Card(4, CardSuit.CLUB), 
            new Card(5, CardSuit.DIAMOND)});
        assertFalse(straightFlushChecker.check(pokerHand));
    }
    
}
