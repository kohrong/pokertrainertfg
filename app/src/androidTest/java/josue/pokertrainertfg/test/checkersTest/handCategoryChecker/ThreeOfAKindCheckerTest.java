package josue.pokertrainertfg.test.checkersTest.handCategoryChecker;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.checkers.handCategoryChecker.ThreeOfAKindChecker;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.PokerHand;


public class ThreeOfAKindCheckerTest extends InstrumentationTestCase {
    
    public ThreeOfAKindCheckerTest() {
    }


    public void testCheck() throws Card.Exception {
        ThreeOfAKindChecker threeOfAKindChecker = new ThreeOfAKindChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card(1, CardSuit.CLUB),
            new Card(2, CardSuit.CLUB), new Card(3, CardSuit.CLUB), new Card(4, CardSuit.CLUB),
            new Card(7, CardSuit.DIAMOND)});
        assertFalse(threeOfAKindChecker.check(pokerHand));
    }
    

    public void testIsThreeOfAKind() throws Card.Exception{
        ThreeOfAKindChecker threeOfAKindChecker = new ThreeOfAKindChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card(1, CardSuit.CLUB), 
            new Card(1, CardSuit.DIAMOND), new Card(1, CardSuit.HEART), new Card(4, CardSuit.CLUB), 
            new Card(7, CardSuit.DIAMOND)});
        assertTrue(threeOfAKindChecker.check(pokerHand));
    }
    

    public void testIsAFourOfAKind() throws Card.Exception{
        ThreeOfAKindChecker threeOfAKindChecker = new ThreeOfAKindChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card(1, CardSuit.CLUB), 
            new Card(1, CardSuit.DIAMOND), new Card(1, CardSuit.HEART), new Card(1, CardSuit.SPADE), 
            new Card(7, CardSuit.DIAMOND)});
        assertTrue(threeOfAKindChecker.check(pokerHand));
    }
    
}
