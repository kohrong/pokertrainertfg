package josue.pokertrainertfg.test.checkersTest.subHandCategoryChecker;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.checkers.subHandCategoryChecker.MediumPairSecondKickerChecker;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.TexasHand;


/**
 * Created by Josue on 26/08/14.
 */
public class MediumPairSecondKickerCheckerTest extends InstrumentationTestCase {

    public void testIsMMediumPairFirstKicker() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('a', CardSuit.CLUB), new Card(9, CardSuit.CLUB));

        Board board = new Board(new Card[]{new Card(3, CardSuit.HEART), new Card(3, CardSuit.DIAMOND),
                new Card(2, CardSuit.SPADE)}, new Card(8, CardSuit.HEART));

        assertTrue(new MediumPairSecondKickerChecker().check(texasHand, board));
    }

    public void testIsMediumPairSecondKicker() throws Card.Exception{
        TexasHand texasHand = new TexasHand(new Card(8, CardSuit.CLUB), new Card('k', CardSuit.CLUB));

        Board board = new Board(new Card[]{new Card('a', CardSuit.HEART), new Card(4, CardSuit.DIAMOND),
                new Card(8, CardSuit.SPADE)}, new Card('j', CardSuit.HEART));

        assertTrue(new MediumPairSecondKickerChecker().check(texasHand, board));
    }

    public void testIsMediumPairBestsKickersOnBoard() throws Card.Exception{
        TexasHand texasHand = new TexasHand(new Card(9, CardSuit.CLUB), new Card(8, CardSuit.CLUB));

        Board board = new Board(new Card[]{new Card('j', CardSuit.HEART), new Card('q', CardSuit.DIAMOND),
                new Card('a', CardSuit.SPADE)}, new Card('k', CardSuit.HEART), new Card('k', CardSuit.HEART));

        assertTrue(new MediumPairSecondKickerChecker().check(texasHand, board));
    }

    public void testIsNotMediumPairSecondKicker() throws Card.Exception{
        TexasHand texasHand = new TexasHand(new Card(9, CardSuit.CLUB), new Card(8, CardSuit.CLUB));

        Board board = new Board(new Card[]{new Card(3, CardSuit.HEART), new Card('q', CardSuit.DIAMOND),
                new Card('a', CardSuit.SPADE)}, new Card('a', CardSuit.HEART), new Card('k', CardSuit.HEART));

        assertFalse(new MediumPairSecondKickerChecker().check(texasHand, board));
    }
}
