package josue.pokertrainertfg.test.checkersTest.handCategoryChecker;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.checkers.handCategoryChecker.HighCardChecker;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.PokerHand;


public class HighCardCheckerTest extends InstrumentationTestCase {
    
    public HighCardCheckerTest() {
    }


    public void testCheck() throws Card.Exception {
        HighCardChecker highCardChecker = new HighCardChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card(1, CardSuit.CLUB),
            new Card(2, CardSuit.CLUB), new Card(3, CardSuit.CLUB), new Card(4, CardSuit.CLUB), 
            new Card(7, CardSuit.DIAMOND)});
        assertTrue(highCardChecker.check(pokerHand));
    }
    
}
