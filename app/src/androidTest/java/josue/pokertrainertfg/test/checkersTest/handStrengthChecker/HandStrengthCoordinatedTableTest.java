package josue.pokertrainertfg.test.checkersTest.handStrengthChecker;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.checkers.handStrengthChecker.HandStrength;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.TexasHand;


/**
 * Created by Josue on 28/08/14.
 */
public class HandStrengthCoordinatedTableTest extends InstrumentationTestCase {

    public void testIsMediumStrength() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('a', CardSuit.SPADE), new Card('a', CardSuit.DIAMOND));

        Board board = new Board(new Card[]{new Card('q', CardSuit.SPADE), new Card('t', CardSuit.HEART),
            new Card(8, CardSuit.HEART)}, new Card(6, CardSuit.CLUB));

        assertTrue(new HandStrength().getStrength(texasHand, board) == HandStrength.Strength.medium);
    }

    public void testIsStrongHandAndGoesToMedium() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('j', CardSuit.DIAMOND), new Card(9, CardSuit.SPADE));

        Board board = new Board(new Card[]{new Card('j', CardSuit.SPADE), new Card(9, CardSuit.CLUB),
                new Card(8, CardSuit.HEART)});

        assertTrue(new HandStrength().getStrength(texasHand, board) == HandStrength.Strength.strong);

        board.setCard(new Card(8, CardSuit.DIAMOND));
        assertTrue(new HandStrength().getStrength(texasHand, board) == HandStrength.Strength.medium);
    }

    public void testIsVeryStrongHand() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('t', CardSuit.HEART), new Card(7, CardSuit.HEART));

        Board board = new Board(new Card[]{new Card('j', CardSuit.SPADE), new Card(9, CardSuit.SPADE),
                new Card(8, CardSuit.HEART)});

        assertTrue(new HandStrength().getStrength(texasHand, board) == HandStrength.Strength.veryStrong);
    }

    public void testIsWeakHand() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('a', CardSuit.SPADE), new Card('k', CardSuit.DIAMOND));

        Board board = new Board(new Card[]{new Card('q', CardSuit.CLUB), new Card(9, CardSuit.DIAMOND),
                new Card(8, CardSuit.HEART)}, new Card(7, CardSuit.DIAMOND));

        assertTrue(new HandStrength().getStrength(texasHand, board) == HandStrength.Strength.weak);
    }

    public void testIsNothingHand() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('t', CardSuit.DIAMOND), new Card('k', CardSuit.DIAMOND));

        Board board = new Board(new Card[]{new Card(7, CardSuit.DIAMOND), new Card(6, CardSuit.DIAMOND),
                new Card(8, CardSuit.HEART)});

        assertTrue(new HandStrength().getStrength(texasHand, board) == HandStrength.Strength.nothing);
    }
}
