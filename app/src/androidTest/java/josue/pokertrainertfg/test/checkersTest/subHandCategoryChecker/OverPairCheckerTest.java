package josue.pokertrainertfg.test.checkersTest.subHandCategoryChecker;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.checkers.subHandCategoryChecker.OverPairChecker;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.TexasHand;


/**
 * Created by Josue on 26/08/14.
 */
public class OverPairCheckerTest extends InstrumentationTestCase {

    public void testIsOverpair() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('a', CardSuit.HEART), new Card('a', CardSuit.SPADE));

        Board board = new Board(new Card[]{new Card('k', CardSuit.CLUB), new Card(8, CardSuit.CLUB),
            new Card(2, CardSuit.DIAMOND)}, new Card('j', CardSuit.DIAMOND));

        assertTrue(new OverPairChecker().check(texasHand, board));
    }

    public void testIsOverpairLowBoard() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('a', CardSuit.HEART), new Card('a', CardSuit.SPADE));

        Board board = new Board(new Card[]{new Card(3, CardSuit.CLUB), new Card(8, CardSuit.CLUB),
                new Card(2, CardSuit.DIAMOND)}, new Card(4, CardSuit.DIAMOND));

        assertTrue(new OverPairChecker().check(texasHand, board));
    }
}
