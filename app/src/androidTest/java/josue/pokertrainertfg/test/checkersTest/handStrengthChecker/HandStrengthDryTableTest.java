package josue.pokertrainertfg.test.checkersTest.handStrengthChecker;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.checkers.handStrengthChecker.HandStrength;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.TexasHand;


/**
 * Created by Josue on 28/08/14.
 */
public class HandStrengthDryTableTest extends InstrumentationTestCase {

    public void testIsStrongHand() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('k', CardSuit.DIAMOND), new Card('q', CardSuit.DIAMOND));

        Board board = new Board(new Card[]{new Card('q', CardSuit.SPADE), new Card(8, CardSuit.DIAMOND),
                new Card(3, CardSuit.HEART)});

        assertTrue(new HandStrength().getStrength(texasHand, board) == HandStrength.Strength.strong);
    }

    public void testIsMediumHand() throws Card.Exception{
        TexasHand texasHand = new TexasHand(new Card('a', CardSuit.SPADE), new Card('8', CardSuit.SPADE));

        Board board = new Board(new Card[]{new Card('k', CardSuit.SPADE), new Card(8, CardSuit.DIAMOND),
                new Card(6, CardSuit.HEART)});

        assertTrue(new HandStrength().getStrength(texasHand, board) == HandStrength.Strength.medium);
    }

    public void testIsWeakHand() throws Card.Exception{
        TexasHand texasHand = new TexasHand(new Card('k', CardSuit.HEART), new Card('8', CardSuit.HEART));

        Board board = new Board(new Card[]{new Card('a', CardSuit.HEART), new Card(9, CardSuit.CLUB),
                new Card(4, CardSuit.SPADE)});

        assertTrue(new HandStrength().getStrength(texasHand, board) == HandStrength.Strength.weak);
    }

    public void testIsVeryStrongHand() throws Card.Exception{
        TexasHand texasHand = new TexasHand(new Card('a', CardSuit.CLUB), new Card('q', CardSuit.HEART));

        Board board = new Board(new Card[]{new Card('q', CardSuit.DIAMOND), new Card('a', CardSuit.HEART),
                new Card(2, CardSuit.HEART)});

        assertTrue(new HandStrength().getStrength(texasHand, board) == HandStrength.Strength.veryStrong);
    }

    public void testIsNothingHand() throws Card.Exception{
        TexasHand texasHand = new TexasHand(new Card('k', CardSuit.SPADE), new Card('j', CardSuit.CLUB));

        Board board = new Board(new Card[]{new Card('q', CardSuit.DIAMOND), new Card(9, CardSuit.CLUB),
                new Card(4, CardSuit.SPADE)});

        assertTrue(new HandStrength().getStrength(texasHand, board) == HandStrength.Strength.nothing);
    }
}
