package josue.pokertrainertfg.test.checkersTest.subHandCategoryChecker;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.checkers.subHandCategoryChecker.MaxPairSecondKickerChecker;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.TexasHand;


/**
 * Created by Josue on 26/08/14.
 */
public class MaxPairSecondKickerCheckerTest extends InstrumentationTestCase {

    public void testIsMaxPairFirstKicker() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('a', CardSuit.CLUB), new Card('k', CardSuit.CLUB));

        Board board = new Board(new Card[]{new Card(3, CardSuit.HEART), new Card(8, CardSuit.DIAMOND),
        new Card(8, CardSuit.SPADE)});

        assertTrue(new MaxPairSecondKickerChecker().check(texasHand, board));
    }

    public void testIsMaxPairSecondKicker() throws Card.Exception{
        TexasHand texasHand = new TexasHand(new Card('q', CardSuit.CLUB), new Card('k', CardSuit.CLUB));

        Board board = new Board(new Card[]{new Card(3, CardSuit.HEART), new Card(8, CardSuit.DIAMOND),
                new Card(8, CardSuit.SPADE)});

        assertTrue(new MaxPairSecondKickerChecker().check(texasHand, board));
    }

    public void testIsMaxPairSecondKickerWithFirstKicker() throws Card.Exception{
        TexasHand texasHand = new TexasHand(new Card('q', CardSuit.CLUB), new Card('k', CardSuit.CLUB));

        Board board = new Board(new Card[]{new Card(3, CardSuit.HEART), new Card(8, CardSuit.DIAMOND),
                new Card('a', CardSuit.SPADE)}, new Card('a', CardSuit.HEART));

        assertTrue(new MaxPairSecondKickerChecker().check(texasHand, board));
    }

    public void testIsNotMaxPairSecondKicker() throws Card.Exception{
        TexasHand texasHand = new TexasHand(new Card(9, CardSuit.CLUB), new Card(8, CardSuit.CLUB));

        Board board = new Board(new Card[]{new Card(3, CardSuit.HEART), new Card('q', CardSuit.DIAMOND),
                new Card('a', CardSuit.SPADE)}, new Card('a', CardSuit.HEART), new Card('k', CardSuit.HEART));

        assertFalse(new MaxPairSecondKickerChecker().check(texasHand, board));
    }

    public void testIsMaxPairFirstKickerLowCards() throws Card.Exception{
        TexasHand texasHand = new TexasHand(new Card(2, CardSuit.CLUB), new Card('a', CardSuit.CLUB));

        Board board = new Board(new Card[]{new Card(8, CardSuit.HEART), new Card(8, CardSuit.DIAMOND),
                new Card(7, CardSuit.SPADE)}, new Card(6, CardSuit.HEART), new Card(5, CardSuit.HEART));

        assertTrue(new MaxPairSecondKickerChecker().check(texasHand, board));
    }

    public void testIsMaxPairFirstKickerKing() throws Card.Exception{
        TexasHand texasHand = new TexasHand(new Card('k', CardSuit.CLUB), new Card('a', CardSuit.CLUB));

        Board board = new Board(new Card[]{new Card('a', CardSuit.HEART), new Card(2, CardSuit.DIAMOND),
                new Card(3, CardSuit.SPADE)}, new Card(5, CardSuit.HEART));

        assertTrue(new MaxPairSecondKickerChecker().check(texasHand, board));
    }

    public void testIsMaxPairFirstKickerQueen() throws Card.Exception{
        TexasHand texasHand = new TexasHand(new Card('q', CardSuit.CLUB), new Card('a', CardSuit.CLUB));

        Board board = new Board(new Card[]{new Card('a', CardSuit.HEART), new Card(2, CardSuit.DIAMOND),
                new Card(3, CardSuit.SPADE)}, new Card(5, CardSuit.HEART));

        assertTrue(new MaxPairSecondKickerChecker().check(texasHand, board));
    }
}
