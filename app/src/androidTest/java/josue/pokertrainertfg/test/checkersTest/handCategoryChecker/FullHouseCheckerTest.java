package josue.pokertrainertfg.test.checkersTest.handCategoryChecker;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.checkers.handCategoryChecker.FullHouseChecker;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.PokerHand;


public class FullHouseCheckerTest extends InstrumentationTestCase {
    
    public FullHouseCheckerTest() {
    }

    public void testCheck() throws Card.Exception {
        FullHouseChecker fullHouseChecker = new FullHouseChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card(1, CardSuit.CLUB),
            new Card(1, CardSuit.DIAMOND), new Card(1, CardSuit.HEART), new Card(4, CardSuit.CLUB),
            new Card(7, CardSuit.DIAMOND)});
        assertFalse(fullHouseChecker.check(pokerHand));
    }

    public void testIsFullHouse() throws Card.Exception{
        FullHouseChecker fullHouseChecker = new FullHouseChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card(1, CardSuit.CLUB), 
            new Card(1, CardSuit.DIAMOND), new Card(1, CardSuit.HEART), new Card(4, CardSuit.CLUB), 
            new Card(4, CardSuit.DIAMOND)});
        assertTrue(fullHouseChecker.check(pokerHand));
    }

    public void testIsFourOfAKind() throws Card.Exception{
        FullHouseChecker fullHouseChecker = new FullHouseChecker();
        PokerHand pokerHand = new PokerHand(new Card[]{new Card(1, CardSuit.CLUB), 
            new Card(1, CardSuit.DIAMOND), new Card(1, CardSuit.HEART), new Card(1, CardSuit.CLUB), 
            new Card(4, CardSuit.DIAMOND)});
        assertFalse(fullHouseChecker.check(pokerHand));
    }
}
