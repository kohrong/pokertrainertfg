package josue.pokertrainertfg.test.checkersTest.subHandCategoryChecker;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.checkers.subHandCategoryChecker.MaximumFullHouseChecker;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.TexasHand;


/**
 * Created by Josue on 26/08/14.
 */
public class MaximumFullHouseCheckerTest extends InstrumentationTestCase{

    public void testIsMaximumFullHouseWithTwoCards() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card('k', CardSuit.DIAMOND), new Card('k', CardSuit.CLUB));

        Board board = new Board(new Card[]{new Card('k', CardSuit.HEART), new Card(7, CardSuit.HEART),
        new Card(7, CardSuit.CLUB)});

        assertTrue(new MaximumFullHouseChecker().check(texasHand, board));
    }

    public void testIsMaximumFullHouseWithOneCard() throws Card.Exception {
        TexasHand texasHand = new TexasHand(new Card(7, CardSuit.DIAMOND), new Card('k', CardSuit.CLUB));

        Board board = new Board(new Card[]{new Card('k', CardSuit.HEART), new Card(7, CardSuit.HEART),
                new Card(7, CardSuit.CLUB)}, new Card('k', CardSuit.DIAMOND));

        assertTrue(new MaximumFullHouseChecker().check(texasHand, board));
    }
}
