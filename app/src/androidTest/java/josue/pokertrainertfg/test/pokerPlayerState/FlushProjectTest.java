package josue.pokertrainertfg.test.pokerPlayerState;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.TexasHand;
import josue.pokertrainer.utils.FlushProject;


public class FlushProjectTest extends InstrumentationTestCase {


	public void testMyPokerHandIsFlush() throws Exception {
		Board board = new Board(new Card[]{new Card(7, CardSuit.CLUB), new Card(8, CardSuit.CLUB),
				new Card(9, CardSuit.CLUB)});
		TexasHand texasHand = new TexasHand(new Card(3, CardSuit.CLUB), new Card(4, CardSuit.CLUB));
		
		assertFalse(new FlushProject(board, texasHand).isFlushProject());
	}
	

	public void testIsFlushProjectOffSuited() throws Exception{
		Board board = new Board(new Card[]{new Card(7, CardSuit.CLUB), new Card(8, CardSuit.CLUB),
				new Card(9, CardSuit.CLUB)});
		TexasHand texasHand = new TexasHand(new Card(3, CardSuit.DIAMOND), new Card(4, CardSuit.CLUB));
		
		assertTrue(new FlushProject(board, texasHand).isFlushProject());
	}
	

	public void testIsFlushProjectSuited() throws Exception{
		Board board = new Board(new Card[]{new Card(7, CardSuit.CLUB), new Card(8, CardSuit.CLUB),
				new Card(9, CardSuit.DIAMOND)});
		TexasHand texasHand = new TexasHand(new Card(3, CardSuit.CLUB), new Card(4, CardSuit.CLUB));
		
		assertTrue(new FlushProject(board, texasHand).isFlushProject());
	}

}
