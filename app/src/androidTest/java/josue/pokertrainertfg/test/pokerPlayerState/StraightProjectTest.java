package josue.pokertrainertfg.test.pokerPlayerState;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.TexasHand;
import josue.pokertrainer.utils.StraightProject;

public class StraightProjectTest extends InstrumentationTestCase {


	public void testIsNotStraightProject() throws Exception {
		Board board = new Board(new Card[]{new Card(7, CardSuit.CLUB), new Card(8, CardSuit.CLUB),
				new Card(9, CardSuit.CLUB)});
		TexasHand texasHand = new TexasHand(new Card(3, CardSuit.CLUB), new Card(4, CardSuit.CLUB));
		
		assertFalse(new StraightProject(board, texasHand).isStraightProject());
	}
	

	public void testIsStraightProject() throws Exception{
		Board board = new Board(new Card[]{new Card(7, CardSuit.CLUB), new Card(6, CardSuit.CLUB),
				new Card(10, CardSuit.CLUB)});
		TexasHand texasHand = new TexasHand(new Card(3, CardSuit.CLUB), new Card(4, CardSuit.CLUB));
		
		assertTrue(new StraightProject(board, texasHand).isStraightProject());
	}


	public void testIsSpecialStraightProject() throws Exception{
		Board board = new Board(new Card[]{new Card('a', CardSuit.CLUB), new Card(2, CardSuit.CLUB),
				new Card(10, CardSuit.CLUB)});
		TexasHand texasHand = new TexasHand(new Card(3, CardSuit.CLUB), new Card(4, CardSuit.CLUB));
		
		assertTrue(new StraightProject(board, texasHand).isStraightProject());
	}
	

	public void testIsRoyalStraightProject() throws Exception{
		Board board = new Board(new Card[]{new Card('a', CardSuit.CLUB), new Card('j', CardSuit.CLUB),
				new Card('k', CardSuit.CLUB)});
		TexasHand texasHand = new TexasHand(new Card('q', CardSuit.CLUB), new Card(4, CardSuit.CLUB));
		
		assertTrue(new StraightProject(board, texasHand).isStraightProject());
	}
	

	public void testIsNotStraightProjectCardsRepeated() throws Exception{
		Board board = new Board(new Card[]{new Card(7, CardSuit.CLUB), new Card(7, CardSuit.CLUB),
				new Card(10, CardSuit.CLUB)});
		TexasHand texasHand = new TexasHand(new Card(3, CardSuit.CLUB), new Card(4, CardSuit.CLUB));
		
		assertFalse(new StraightProject(board, texasHand).isStraightProject());
	}
	

	public void testIsRoyalStraight() throws Exception{
		TexasHand texasHand = new TexasHand(new Card('a', CardSuit.DIAMOND), new Card('k', CardSuit.DIAMOND));
		
		Board board = new Board(new Card[]{new Card('q', CardSuit.HEART), new Card('j', CardSuit.DIAMOND),
				new Card(10, CardSuit.SPADE)});
		
		assertFalse(new StraightProject(board, texasHand).isStraightProject());
	}
	

	public void testIsSpecialStraight() throws Exception{
		TexasHand texasHand = new TexasHand(new Card('a', CardSuit.CLUB), new Card(5, CardSuit.CLUB));
		
		Board board = new Board(new Card[]{new Card(2, CardSuit.HEART), new Card(3, CardSuit.HEART),
				new Card(4, CardSuit.DIAMOND)});
		
		assertFalse(new StraightProject(board, texasHand).isStraightProject());
	}
	

	public void testIsSpecialStraightProjectWithAKing() throws Exception{
		TexasHand texasHand = new TexasHand(new Card('a', CardSuit.CLUB), new Card('k', CardSuit.CLUB));
		
		Board board = new Board(new Card[]{new Card(2, CardSuit.HEART), new Card(3, CardSuit.HEART),
				new Card(4, CardSuit.DIAMOND)});
		
		assertTrue(new StraightProject(board, texasHand).isStraightProject());
	}
	
}
