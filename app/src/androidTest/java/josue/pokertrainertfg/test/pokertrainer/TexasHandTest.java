package josue.pokertrainertfg.test.pokertrainer;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.TexasHand;

public class TexasHandTest extends InstrumentationTestCase {
    

    public void testValues() throws Card.Exception{
        Card card = new Card(13, CardSuit.HEART);
        assertTrue(card.getValue()== 13);
        
        card = new Card('a', CardSuit.CLUB);
        assertTrue(card.getValue()== 14);
    }
    

    public void testIsSuited() throws Card.Exception {
        assertTrue(new TexasHand(new Card(1, CardSuit.CLUB), new Card(2, CardSuit.CLUB)).isSuited());
        assertFalse(new TexasHand(new Card('j', CardSuit.SPADE), new Card('4', CardSuit.CLUB)).isSuited());
    }
    

    public void testIsConnected() throws Card.Exception {
        assertTrue(new TexasHand(new Card(2, CardSuit.CLUB), new Card(3, CardSuit.CLUB)).isConnected());
        assertFalse(new TexasHand(new Card(4, CardSuit.CLUB), new Card('k', CardSuit.CLUB)).isConnected());
        assertTrue(new TexasHand(new Card('a', CardSuit.CLUB), new Card(5, CardSuit.HEART)).isConnected());
        assertTrue(new TexasHand(new Card('a', CardSuit.CLUB), new Card('t', CardSuit.HEART)).isConnected());
    }
    

    public void testIsPocketPair() throws Card.Exception {
        assertTrue(new TexasHand(new Card(5, CardSuit.CLUB), new Card('5', CardSuit.CLUB)).isPocketPair());
        assertFalse(new TexasHand(new Card(5, CardSuit.CLUB), new Card('k', CardSuit.CLUB)).isPocketPair());
    }
    
    public void testIsBroadway() throws Card.Exception {
        assertTrue(new TexasHand(new Card('a', CardSuit.CLUB), new Card('k', CardSuit.CLUB)).isBroadway());
        assertFalse(new TexasHand(new Card(5, CardSuit.CLUB), new Card('5', CardSuit.CLUB)).isBroadway());
    }
}
