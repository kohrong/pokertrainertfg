package josue.pokertrainertfg.test.pokertrainer;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.Deck;
import josue.pokertrainer.model.PokerHand;


public class DeckTest extends InstrumentationTestCase{


    public void testIterator() throws Card.Exception {
        Deck deck = new Deck();
        int index = 1;
        int suitIndex = 0;
        for (Card card : deck) {
            assertTrue(card.getNumber() == index++);
            assertTrue(card.getSuit() == CardSuit.SUITS[suitIndex]);
            if (index <= 13) continue;
            index = 1;
            suitIndex++;
        }
    }
    

    public void testDeckSize() throws Card.Exception{
        Deck deck = new Deck();
        assertTrue(deck.getCardList().size() == 52);
    }
    

    public void testIsCardInDeck() throws Card.Exception{
        Deck deck = new Deck();
        Card card = new Card(1, CardSuit.CLUB);
        assertTrue(deck.isInDeck(card));
    }
    

    public void testTakeCard() throws Card.Exception{
        Deck deck = new Deck();
        
        assertTrue(deck.takeCard(1, CardSuit.CLUB).isSameCard(new Card(1, CardSuit.CLUB)));
        assertTrue(deck.getCardList().size() == 51);
        
        assertFalse(deck.isInDeck(new Card(1, CardSuit.CLUB)));
    }
    

    public void testTakeRandomCard() throws Card.Exception{
        Deck deck = new Deck();
        
        Card card = deck.takeRandomCard();
        assertTrue(deck.getCardList().size() == 51);
        assertFalse(deck.isInDeck(card));
        
        Deck deck2 = new Deck();
        assertTrue(deck2.isInDeck(card));
    }
    

    public void testCreatePokerHand() throws Card.Exception{
        Deck deck = new Deck();
        PokerHand pokerHand = deck.createPokerHand();
        assertTrue(pokerHand.getSize() == 5);
    }
}
