package josue.pokertrainertfg.test.table;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.checkers.handCategoryChecker.StraightChecker;
import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.CardSuit;
import josue.pokertrainer.model.Player;
import josue.pokertrainer.model.PokerHand;
import josue.pokertrainer.model.Table;
import josue.pokertrainer.model.TexasHand;
import josue.pokertrainer.utils.PokerHandCalculator;


public class CalculatePokerHandTest extends InstrumentationTestCase {
	Table table = new Table(null, null);


	public void testPreFlop() throws Exception {	
		Board board = new Board();
		table.setBoard(board);
		
		Player player = new Player("Josue", new TexasHand(new Card(7, CardSuit.CLUB), new Card(8, CardSuit.DIAMOND)));
		
		PokerHandCalculator boardChecker = new PokerHandCalculator();
		PokerHand pokerHand = boardChecker.calculate(player.getTexasHand(), board);
		
		assertTrue(pokerHand == null);
	}
	

	public void testOnFlop() throws Exception {
		Board board = new Board(new Card[]{new Card(4, CardSuit.SPADE), new Card(5, CardSuit.DIAMOND), 
				new Card(6, CardSuit.CLUB)});
		table.setBoard(board);
		
		Player player = new Player("Josue", new TexasHand(new Card(7, CardSuit.CLUB), new Card(8, CardSuit.DIAMOND)));
		
		PokerHandCalculator boardChecker = new PokerHandCalculator();
		PokerHand pokerHand = boardChecker.calculate(player.getTexasHand(), board);
		
		player.setPokerHand(pokerHand);
		
		StraightChecker straightChecker = new StraightChecker();
		assertTrue(straightChecker.check(player.getPokerHand()));
	}
	

	public void testOnTurn() throws Exception{
		Board board = new Board(new Card[]{new Card(4, CardSuit.SPADE), new Card(5, CardSuit.DIAMOND), 
				new Card(6, CardSuit.CLUB)}, new Card(9, CardSuit.HEART));
		table.setBoard(board);
		
		Player player = new Player("Josue", new TexasHand(new Card(7, CardSuit.CLUB), new Card(8, CardSuit.DIAMOND)));
		
		
		
		PokerHandCalculator boardChecker = new PokerHandCalculator();
		PokerHand pokerHand = boardChecker.calculate(player.getTexasHand(), board);
		
		player.setPokerHand(pokerHand);
		
		StraightChecker straightChecker = new StraightChecker();
		assertTrue(straightChecker.check(player.getPokerHand()));
		
		assertTrue(pokerHand.getCard(0).getValue() == 9);
	}


	public void testOnRiver() throws Exception{
		Board board = new Board(new Card[]{new Card(4, CardSuit.SPADE), new Card(5, CardSuit.DIAMOND), 
				new Card(6, CardSuit.CLUB)}, new Card(9, CardSuit.HEART), new Card(10, CardSuit.CLUB));
		table.setBoard(board);
		
		Player player = new Player("Josue", new TexasHand(new Card(7, CardSuit.CLUB), new Card(8, CardSuit.DIAMOND)));
		
		
		
		PokerHandCalculator boardChecker = new PokerHandCalculator();
		PokerHand pokerHand = boardChecker.calculate(player.getTexasHand(), board);
		
		player.setPokerHand(pokerHand);
		
		StraightChecker straightChecker = new StraightChecker();
		
		assertTrue(straightChecker.check(player.getPokerHand()));
		assertTrue(player.getPokerHand().getCard(0).getValue() == 10);
	}
}
