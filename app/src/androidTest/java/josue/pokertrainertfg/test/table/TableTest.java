package josue.pokertrainertfg.test.table;

import android.test.InstrumentationTestCase;

import josue.pokertrainer.model.Board;
import josue.pokertrainer.model.Card;
import josue.pokertrainer.model.Deck;
import josue.pokertrainer.model.Table;


public class TableTest extends InstrumentationTestCase {


	public void testPutFlop() throws Exception {
		Table table = new Table(null, null);
		Board board = new Board();
		Deck deck = new Deck();
		
		table.setDeck(deck);
		table.setBoard(board);
		
		assertTrue(table.getBoard().isPreFlop());

        Card[] cards = new Card[3];
        for (int i = 0; i < cards.length; i++) {
            cards[i] = deck.takeRandomCard();
        }

        table.getBoard().setFlop(cards);
		
		assertTrue(table.getBoard().isFlop());
		assertTrue(table.getDeck().getCardList().size() == 49);
	}

}
